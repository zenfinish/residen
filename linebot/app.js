const express = require('express');
const http = require('http');
const https = require('https');

const fs = require('fs');
const privateKey  = fs.readFileSync('/etc/letsencrypt/live/telebot.my.id/privkey.pem', 'utf8');
const certificate = fs.readFileSync('/etc/letsencrypt/live/telebot.my.id/fullchain.pem', 'utf8');
const credentials = { key: privateKey, cert: certificate };

const middleware = require('@line/bot-sdk').middleware;
const JSONParseError = require('@line/bot-sdk').JSONParseError;
const SignatureValidationFailed = require('@line/bot-sdk').SignatureValidationFailed;
const Client = require('@line/bot-sdk').Client;

const app = express();

const config = {
  channelAccessToken: '9iffbi3tvTHkvoZdrAlmsNcH1eKZFx6vXonIO2Naqc+flsQSXNRef1j1xIMh4MG1Io9KSOJO7tKBk89P+u6PC5Ni0X8eSwvxh9Yc+9B/qVZlwQq7gTJvHAC7uzWVei4CS7ebwf+EJNvNX55zJNCg7gdB04t89/1O/w1cDnyilFU=',
  channelSecret: '770b10351ffa5942f53695a41e7b9c0a'
};

app.use(middleware(config));

app.post('/webhook', (req, res) => {
	console.log('wkwkwk=========', Client);
	res.json(req.body.events); // req.body will be webhook event object
});

app.use((err, req, res, next) => {
	if (err instanceof SignatureValidationFailed) {
		console.log('guguk===')
		res.status(401).send(err.signature)
		return
	} else if (err instanceof JSONParseError) {
		console.log('guguk===') 
		res.status(400).send(err.raw)
		return
	}
	next(err) // will throw default 500
});

const httpServer = http.createServer(app);
const httpsServer = https.createServer(credentials, app);

app.get('/', function(req, res){
	res.status(200).json('Masuk Pak..');
});

httpServer.listen(3007, function() {
	console.log('PORT: 3007')
});

httpsServer.listen(3005, function() {
	console.log('PORT: 3005')
});