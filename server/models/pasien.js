const mysql = require('../configs/mysql.js');

class Pasien {

	static save(data) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				INSERT INTO pasien(no_rekmedis, nama, tgl_lahir, sex, diagnosa, dpjp_id, kamar_id, bed, status_id, user_id, tgl)
				VALUES('${data.no_rekmedis}', '${data.nama}', '${data.tgl_lahir}', '${data.sex}', '${JSON.stringify(data.diagnosa)}', '${data.dpjp_id}', '${data.kamar_id}', '${data.bed}', '${data.status_id}', '${data.user_id}', NOW())
			`, function(error, result) {
				if (error) {
					reject(error.message);
				} else {
					resolve(result);
				}
			});
		});
	}

	static find() {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT
					a.id,
					a.nama,
					a.no_rekmedis,
					DATE_FORMAT(a.tgl_lahir, '%Y-%m-%d') AS tgl_lahir,
					ROUND(DATEDIFF(CURRENT_DATE, STR_TO_DATE(a.tgl_lahir, '%Y-%m-%d'))/365, 0) AS usia,
					DATE_FORMAT(a.tgl, '%d %M %Y %T') AS tgl,
					a.sex,
					a.diagnosa,
					a.kamar_id,
					a.residen_id,
					a.bed,
					a.keterangan,
					a.status_id,
					b.nama AS dpjp_nama,
					c.nama AS kamar_nama,
					d.nama AS status_nama,
					e.nama AS residen_nama
				FROM pasien a
				LEFT JOIN data_dpjp b ON a.dpjp_id = b.id
				LEFT JOIN data_kamar c ON a.kamar_id = c.id
				LEFT JOIN data_status d ON a.status_id = d.id
				LEFT JOIN data_residen e ON a.residen_id = e.id
				WHERE a.tgl_checkout = '0000-00-00 00:00'
				ORDER BY a.id DESC
			`, function(error, result) {
				if (error) {
					reject(error.message);
				} else {
					resolve(result);
				}
			});
		});
	}

	static findByResiden(residen_id) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT
					a.id,
					a.nama,
					a.no_rekmedis,
					DATE_FORMAT(a.tgl_lahir, '%Y-%m-%d') AS tgl_lahir,
					ROUND(DATEDIFF(CURRENT_DATE, STR_TO_DATE(a.tgl_lahir, '%Y-%m-%d'))/365, 0) AS usia,
					DATE_FORMAT(a.tgl, '%d %M %Y %T') AS tgl,
					a.sex,
					a.diagnosa,
					a.kamar_id,
					a.residen_id,
					a.bed,
					a.keterangan,
					b.nama AS dpjp_nama,
					c.nama AS kamar_nama,
					d.nama AS status_nama,
					e.nama AS residen_nama
				FROM pasien a
				LEFT JOIN data_dpjp b ON a.dpjp_id = b.id
				LEFT JOIN data_kamar c ON a.kamar_id = c.id
				LEFT JOIN data_status d ON a.status_id = d.id
				LEFT JOIN data_residen e ON a.residen_id = e.id
				WHERE a.tgl_checkout = '0000-00-00 00:00' && a.residen_id = '${residen_id}'
				ORDER BY a.id DESC
			`, function(error, result) {
				if (error) {
					reject(error.message);
				} else {
					resolve(result);
				}
			});
		});
	}

	static findByDpjp(dpjp_id) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT
					a.id,
					a.nama,
					a.no_rekmedis,
					DATE_FORMAT(a.tgl_lahir, '%Y-%m-%d') AS tgl_lahir,
					ROUND(DATEDIFF(CURRENT_DATE, STR_TO_DATE(a.tgl_lahir, '%Y-%m-%d'))/365, 0) AS usia,
					DATE_FORMAT(a.tgl, '%d %M %Y %T') AS tgl,
					a.sex,
					a.diagnosa,
					a.kamar_id,
					a.residen_id,
					a.bed,
					a.keterangan,
					a.status_id,
					b.nama AS dpjp_nama,
					c.nama AS kamar_nama,
					d.nama AS status_nama,
					e.nama AS residen_nama
				FROM pasien a
				LEFT JOIN data_dpjp b ON a.dpjp_id = b.id
				LEFT JOIN data_kamar c ON a.kamar_id = c.id
				LEFT JOIN data_status d ON a.status_id = d.id
				LEFT JOIN data_residen e ON a.residen_id = e.id
				WHERE a.tgl_checkout = '0000-00-00 00:00' && a.dpjp_id = '${dpjp_id}'
				ORDER BY a.id DESC
			`, function(error, result) {
				if (error) {
					reject(error.message);
				} else {
					resolve(result);
				}
			});
		});
	}

	static findByStatus(status_id) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT
					a.id,
					a.nama,
					a.no_rekmedis,
					DATE_FORMAT(a.tgl_lahir, '%Y-%m-%d') AS tgl_lahir,
					ROUND(DATEDIFF(CURRENT_DATE, STR_TO_DATE(a.tgl_lahir, '%Y-%m-%d'))/365, 0) AS usia,
					DATE_FORMAT(a.tgl, '%d %M %Y %T') AS tgl,
					a.sex,
					a.diagnosa,
					a.kamar_id,
					a.residen_id,
					a.bed,
					a.keterangan,
					a.status_id,
					b.nama AS dpjp_nama,
					c.nama AS kamar_nama,
					d.nama AS status_nama,
					e.nama AS residen_nama
				FROM pasien a
				LEFT JOIN data_dpjp b ON a.dpjp_id = b.id
				LEFT JOIN data_kamar c ON a.kamar_id = c.id
				LEFT JOIN data_status d ON a.status_id = d.id
				LEFT JOIN data_residen e ON a.residen_id = e.id
				WHERE a.tgl_checkout = '0000-00-00 00:00' && a.status_id = '${status_id}'
				ORDER BY a.id DESC
			`, function(error, result) {
				if (error) {
					reject(error.message);
				} else {
					resolve(result);
				}
			});
		});
	}

	static findCheckin(no_rekmedis) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT
					COUNT(id) AS jml
				FROM pasien
				WHERE tgl_checkout = '0000-00-00 00:00' && no_rekmedis = '${no_rekmedis}'
			`, function(error, result) {
				if (error) {
					reject(error.message);
				} else {
					resolve(result[0]);
				}
			});
		});
	}

	static checkout(data) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				UPDATE pasien SET
					residen_id = '${data.residen_id}',
					opsi_pulang = '${data.opsi_pulang}',
					tgl_checkout = NOW(),
					user_checkout = '${data.user_checkout}'
				WHERE id = '${data.pasien_id}'
			`, function(error, result) {
				if (error) {
					reject(error.message);
				} else {
					resolve(result);
				}
			});
		});
	}

	static groupResiden() {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT
					COUNT(pasien.id) AS jml,
					data_residen.nama AS residen_nama
				FROM pasien
				LEFT JOIN data_residen ON pasien.residen_id = data_residen.id
				WHERE pasien.tgl_checkout = '0000-00-00 00:00:00'
				GROUP BY pasien.residen_id
			`, function(error, result) {
				if (error) {
					reject(error.message);
				} else {
					resolve(result);
				}
			});
		});
	}

	static updateKamar(pasien_id, kamar_id) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				UPDATE pasien SET kamar_id = '${kamar_id}' WHERE id = '${pasien_id}'
			`, function(error, result) {
				if (error) {
					reject(error.message);
				} else {
					resolve(result);
				}
			});
		});
	}

	static updateResiden(pasien_id, residen_id) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				UPDATE pasien SET residen_id = '${residen_id}' WHERE id = '${pasien_id}'
			`, function(error, result) {
				if (error) {
					reject(error.message);
				} else {
					resolve(result);
				}
			});
		});
	}

	static updateStatus(pasien_id, status_id) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				UPDATE pasien SET status_id = '${status_id}' WHERE id = '${pasien_id}'
			`, function(error, result) {
				if (error) {
					reject(error.message);
				} else {
					resolve(result);
				}
			});
		});
	}

	static updateKeterangan(pasien_id, keterangan) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				UPDATE pasien SET keterangan = '${keterangan}' WHERE id = '${pasien_id}'
			`, function(error, result) {
				if (error) {
					reject(error.message);
				} else {
					resolve(result);
				}
			});
		});
	}

	static updateBed(pasien_id, bed) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				UPDATE pasien SET bed = '${bed}' WHERE id = '${pasien_id}'
			`, function(error, result) {
				if (error) {
					reject(error.message);
				} else {
					resolve(result);
				}
			});
		});
	}

	static updateDiagnosa(pasien_id, diagnosa) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				UPDATE pasien SET diagnosa = '${JSON.stringify(diagnosa)}' WHERE id = '${pasien_id}'
			`, function(error, result) {
				if (error) {
					reject(error.message);
				} else {
					resolve(result);
				}
			});
		});
	}

	static report(data) {
		let kamar = '';
		let residen = '';
		let dpjp = '';
		let status = '';
		let jejaring = '';
		if (data.kamar !== '') {
			kamar = `&& a.kamar_id = '${data.kamar}'`;
		}
		if (data.residen !== '') {
			residen = `&& a.residen_id = '${data.residen}'`;
		}
		if (data.dpjp !== '') {
			dpjp = `&& a.dpjp_id = '${data.dpjp}'`;
		}
		if (data.status !== '') {
			status = `&& a.status_id = '${data.status}'`;
		}
		if (data.jejaring !== '') {
			if (data.jejaring == 'jejaring') {
				jejaring = ` && ((a.kamar_id != '1' && a.kamar_id != '20' && a.kamar_id != '24' && a.status_id = '1') || (a.status_id = '2' && a.kamar_id != '20'))`;
			} else if (data.jejaring == 'nonjejaring') {
				jejaring = ` && (a.kamar_id = '1' || a.kamar_id = '20' || a.kamar_id = '24')`;
			}
		}
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT
					a.id,
					a.nama,
					a.no_rekmedis,
					DATE_FORMAT(a.tgl_lahir, '%Y-%m-%d') AS tgl_lahir,
					ROUND(DATEDIFF(CURRENT_DATE, STR_TO_DATE(a.tgl_lahir, '%Y-%m-%d'))/365, 0) AS usia,
					DATE_FORMAT(a.tgl, '%d %M %Y %T') AS tgl,
					a.sex,
					a.diagnosa,
					a.kamar_id,
					a.residen_id,
					a.bed,
					a.keterangan,
					a.status_id,
					b.nama AS dpjp_nama,
					c.nama AS kamar_nama,
					d.nama AS status_nama,
					e.nama AS residen_nama
				FROM pasien a
				LEFT JOIN data_dpjp b ON a.dpjp_id = b.id
				LEFT JOIN data_kamar c ON a.kamar_id = c.id
				LEFT JOIN data_status d ON a.status_id = d.id
				LEFT JOIN data_residen e ON a.residen_id = e.id
				WHERE a.tgl_checkout = '0000-00-00 00:00' ${kamar} ${residen} ${dpjp} ${status} ${jejaring}
				ORDER BY a.id DESC
			`, function(error, result) {
				if (error) {
					reject(error.message);
				} else {
					resolve(result);
				}
			});
		});
	}

	static updateBiodata(data) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				UPDATE pasien SET
					nama = '${data.nama}',
					no_rekmedis = '${data.no_rekmedis}',
					sex = '${data.sex}',
					tgl_lahir = '${data.tgl_lahir}'
				WHERE id = '${data.pasien_id}'
			`, function(error, result) {
				if (error) {
					reject(error.message);
				} else {
					resolve(result);
				}
			});
		});
	}

	static getJejaring() {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT
					a.id,
					a.nama,
					a.no_rekmedis,
					DATE_FORMAT(a.tgl_lahir, '%Y-%m-%d') AS tgl_lahir,
					ROUND(DATEDIFF(CURRENT_DATE, STR_TO_DATE(a.tgl_lahir, '%Y-%m-%d'))/365, 0) AS usia,
					DATE_FORMAT(a.tgl, '%d %M %Y %T') AS tgl,
					a.sex,
					a.diagnosa,
					a.kamar_id,
					a.residen_id,
					a.bed,
					a.keterangan,
					a.status_id,
					b.nama AS dpjp_nama,
					c.nama AS kamar_nama,
					d.nama AS status_nama,
					e.nama AS residen_nama
				FROM pasien a
				LEFT JOIN data_dpjp b ON a.dpjp_id = b.id
				LEFT JOIN data_kamar c ON a.kamar_id = c.id
				LEFT JOIN data_status d ON a.status_id = d.id
				LEFT JOIN data_residen e ON a.residen_id = e.id
				WHERE
					a.tgl_checkout = '0000-00-00 00:00' &&
					((a.kamar_id != '1' && a.kamar_id != '20' && a.kamar_id != '24' && a.status_id = '1') || (a.status_id = '2' && a.kamar_id != '20'))
				ORDER BY a.id DESC
			`, function(error, result) {
				if (error) {
					reject(error.message);
				} else {
					resolve(result);
				}
			});
		});
	}

	static getNonjejaring() {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT
					a.id,
					a.nama,
					a.no_rekmedis,
					DATE_FORMAT(a.tgl_lahir, '%Y-%m-%d') AS tgl_lahir,
					ROUND(DATEDIFF(CURRENT_DATE, STR_TO_DATE(a.tgl_lahir, '%Y-%m-%d'))/365, 0) AS usia,
					DATE_FORMAT(a.tgl, '%d %M %Y %T') AS tgl,
					a.sex,
					a.diagnosa,
					a.kamar_id,
					a.residen_id,
					a.bed,
					a.keterangan,
					a.status_id,
					b.nama AS dpjp_nama,
					c.nama AS kamar_nama,
					d.nama AS status_nama,
					e.nama AS residen_nama
				FROM pasien a
				LEFT JOIN data_dpjp b ON a.dpjp_id = b.id
				LEFT JOIN data_kamar c ON a.kamar_id = c.id
				LEFT JOIN data_status d ON a.status_id = d.id
				LEFT JOIN data_residen e ON a.residen_id = e.id
				WHERE a.tgl_checkout = '0000-00-00 00:00' && (a.kamar_id = '1' || a.kamar_id = '20' || a.kamar_id = '24')
				ORDER BY a.id DESC
			`, function(error, result) {
				if (error) {
					reject(error.message);
				} else {
					resolve(result);
				}
			});
		});
	}

	static findPasienPulang(data) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT
					a.nama,
					a.no_rekmedis,
					DATE_FORMAT(a.tgl_checkout, '%d %M %Y %T') AS tgl_checkout,
					a.diagnosa,
					b.nama AS dpjp_nama,
					c.nama AS kamar_nama,
					d.nama AS status_nama,
					e.nama AS residen_nama
				FROM pasien a
				LEFT JOIN data_dpjp b ON a.dpjp_id = b.id
				LEFT JOIN data_kamar c ON a.kamar_id = c.id
				LEFT JOIN data_status d ON a.status_id = d.id
				LEFT JOIN data_residen e ON a.residen_id = e.id
				WHERE a.tgl_checkout BETWEEN '${data.tgl_dari}' AND '${data.tgl_sampai}'
				ORDER BY a.tgl_checkout DESC
			`, function(error, result) {
				if (error) {
					reject(error.message);
				} else {
					resolve(result);
				}
			});
		});
	}
	
}

module.exports = Pasien;
