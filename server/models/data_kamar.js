const mysql = require('../configs/mysql.js');

class DataKamar {

	static find() {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT * FROM data_kamar
			`, function(error, result) {
				if (error) {
					reject(error.message);
				} else {
					resolve(result);
				}
			});
		});
	}
	
}

module.exports = DataKamar;
