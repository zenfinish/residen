const mysql = require('../configs/mysql.js');

class DataStatus {

	static find() {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT * FROM data_status
			`, function(error, result) {
				if (error) {
					reject(error.message);
				} else {
					resolve(result);
				}
			});
		});
	}
	
}

module.exports = DataStatus;
