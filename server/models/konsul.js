const mysql = require('../configs/mysql.js');

class Konsul {

	static save(data) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT COUNT(id) AS jml FROM konsul WHERE no_rekmedis = '${data.no_rekmedis}'
			`, function(error, result) {
				if (error) {
					reject(error.message);
				} else {
					if (result[0].jml > 0) {
						reject(`Pasien Masih Ada, Belum Diselesaikan.`);
					} else {
						mysql.query(`
							INSERT INTO konsul(nama, no_rekmedis, ruang, diagnosa, tujuan)
							VALUES('${data.nama}', '${data.no_rekmedis}', '${data.ruang}', '${data.diagnosa}', '${data.tujuan}')
						`, function(error, result) {
							if (error) {
								reject(error.message);
							} else {
								resolve(result);
							}
						});
					}
				}
			});
		});
	}

	static find() {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT *, DATE_FORMAT(konsul.waktu, '%d-%m-%Y %H:%i:%s') AS waktu FROM konsul WHERE status = 0
			`, function(error, result) {
				if (error) {
					reject(error.message);
				} else {
					resolve(result);
				}
			});
		});
	}

	static update(data) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT COUNT(id) AS jml FROM konsul WHERE no_rekmedis = '${data.no_rekmedis}' && status = 0
			`, function(error, result) {
				if (error) {
					reject(error.message);
				} else {
					if (result[0].jml === 0) {
						reject(`Pasien Tidak Ditemukan.`);
					} else {
						mysql.query(`
							UPDATE konsul SET status = 1 WHERE no_rekmedis = '${data.no_rekmedis}'
						`, function(error, result) {
							if (error) {
								reject(error.message);
							} else {
								resolve(result);
							}
						});
					}
				}
			});
		});
	}

	static findByMr(no_rekmedis) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT * FROM konsul WHERE no_rekmedis = '${no_rekmedis}' && status = 0
			`, function(error, result) {
				if (error) {
					reject(error.message);
				} else {
					resolve(result[0]);
				}
			});
		});
	}
	
}

module.exports = Konsul;
