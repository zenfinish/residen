const mysql = require('../configs/mysql.js');

class DataDpjp {

	static find() {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT * FROM data_dpjp
			`, function(error, result) {
				if (error) {
					reject(error.message);
				} else {
					resolve(result);
				}
			});
		});
	}
	
}

module.exports = DataDpjp;
