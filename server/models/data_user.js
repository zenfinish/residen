const mysql = require('../configs/mysql.js');

class DataUser {

	static findOne(username) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT * FROM data_user
				WHERE username = '${username}'
			`, function(error, result) {
				if (error) {
					reject(error.message);
				} else {
					resolve(result[0]);
				}
			});
		});
	}

	static save(data) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				INSERT INTO data_user(telegram, username, nama, password, status)
				VALUES('${data.telegram}', '${data.username}', '${data.nama}', '${data.password}', '${data.status}')
			`, function(error, result) {
				if (error) {
					reject(error.message);
				} else {
					resolve(result);
				}
			});
		});
	}

	static saveLine(data) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				INSERT INTO data_user(line, username, nama, password, status)
				VALUES('${data.line}', '${data.username}', '${data.nama}', '${data.password}', '${data.status}')
			`, function(error, result) {
				if (error) {
					reject(error.message);
				} else {
					resolve(result);
				}
			});
		});
	}
	
}

module.exports = DataUser;
