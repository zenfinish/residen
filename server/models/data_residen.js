const mysql = require('../configs/mysql.js');

class DataResiden {

	static find() {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT * FROM data_residen
			`, function(error, result) {
				if (error) {
					reject(error.message);
				} else {
					resolve(result);
				}
			});
		});
	}
	
}

module.exports = DataResiden;
