const Konsul = require('../models/konsul.js');
const Pasien = require('../models/pasien.js');

class KonsulController {

	static post(req, res) {
		Konsul.save(req.body)
		.then(function(result) {
			res.status(201).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static postPindah(req, res) {
		Konsul.findByMr(req.body.no_rekmedis)
			.then(function(result) {
				Pasien.save({
					...result,
					dpjp_id: req.body.dpjp_id,
					diagnosa: [{ name: result.diagnosa }],
					status_id: 2
				})
					.then(function(result) {
						res.status(201).json(result);
					})
					.catch(function(error) {
						res.status(500).json(error);
					});
			})
			.catch(function(error) {
				res.status(500).json(error);
			});
	}

	static get(req, res) {
		Konsul.find()
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static put(req, res) {
		Konsul.update(req.body)
		.then(function(result) {
			res.status(201).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

}

module.exports = KonsulController;
