const DataDpjp = require('../models/data_dpjp.js');
const Pasien = require('../models/pasien.js');

class DpjpController {

	static get(req, res) {
		DataDpjp.find()
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static getAktif(req, res) {
		Pasien.findByDpjp(req.params.dpjp_id)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

}

module.exports = DpjpController;
