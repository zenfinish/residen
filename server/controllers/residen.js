const DataResiden = require('../models/data_residen.js');
const Pasien = require('../models/pasien.js');

class ResidenController {

	static get(req, res) {
		DataResiden.find()
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static getPasien(req, res) {
		Pasien.groupResiden()
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static getAktif(req, res) {
		Pasien.findByResiden(req.params.residen_id)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

}

module.exports = ResidenController;
