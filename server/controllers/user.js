const DataUser = require('../models/data_user.js');
const { hashingPassword, comparePassword } = require('../configs/bcrypt.js');
const token = require('../configs/jsonwebtoken.js');

class UserController {

	static postRegister(req, res) {
		DataUser.findOne(req.body.username)
		.then(function(result) {
			if (result) {
				res.status(500).json('username sudah ada, silahkan ganti.');
			} else {
				req.body['password'] = hashingPassword(req.body.username);
				DataUser.save(req.body)
				.then(function(result) {
					res.status(200).json('Register Berhasil.');
				})
				.catch(function(error) {
					res.status(500).json(error);
				});
			}
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static postLogin(req, res) {
		DataUser.findOne(req.body.username)
		.then(function(result) {
			if (result) {
				let compare = comparePassword(req.body.password, result.password);
				if (compare) {
					delete result.password;
					token.createToken({ ...result }, function(error, token) {
						if (error) {
							res.status(500).json(error);
						} else {
							res.status(200).json({
								token: token,
								username: req.body.username,
							});
						}
					});
				} else {
					res.status(500).json('Password Salah');
				}
			} else {
				res.status(500).json('Username Tidak Ditemukan');
			}
		})
		.catch(function(error) {
			console.log(error)
			res.status(500).json(error);
		})
	}

	static getCekToken(req, res) {
		res.status(200).json('Berhasil');
	}

}

module.exports = UserController;
