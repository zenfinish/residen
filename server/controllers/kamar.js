const DataKamar = require('../models/data_kamar.js');

class KamarController {

	static get(req, res) {
		DataKamar.find()
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

}

module.exports = KamarController;
