const DataStatus = require('../models/data_status.js');

class StatusController {

	static get(req, res) {
		DataStatus.find()
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

}

module.exports = StatusController;
