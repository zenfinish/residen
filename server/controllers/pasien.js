const Pasien = require('../models/pasien.js');

class PasienController {

	static post(req, res) {
		Pasien.findCheckin(req.body.no_rekmedis)
		.then(function(result) {
			if (result.jml > 0) {
				res.status(500).json(`Pasien Dengan No. Rekmedis ${req.body.no_rekmedis} Belum Di Pulangkan`);
			} else {
				req.body['user_id'] = req.user.id;
				Pasien.save(req.body)
				.then(function(result) {
					res.status(200).json('Berhasil');
				})
				.catch(function(error) {
					res.status(500).json(error);
				});
			}
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static postCheckout(req, res) {
		req.body['user_checkout'] = req.user.id;
		Pasien.checkout(req.body)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static get(req, res) {
		Pasien.find()
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static getDpjp(req, res) {
		Pasien.findByDpjp(req.params.dpjp_id)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static getStatus(req, res) {
		Pasien.findByStatus(req.params.status_id)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static getJejaring(req, res) {
		Pasien.getJejaring()
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static getNonjejaring(req, res) {
		Pasien.getNonjejaring()
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static putKamar(req, res) {
		Pasien.updateKamar(req.params.pasien_id, req.params.kamar_id)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static putResiden(req, res) {
		Pasien.updateResiden(req.params.pasien_id, req.params.residen_id)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static putStatus(req, res) {
		Pasien.updateStatus(req.params.pasien_id, req.params.status_id)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static postKeterangan(req, res) {
		Pasien.updateKeterangan(req.body.pasien_id, req.body.keterangan)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static postBed(req, res) {
		Pasien.updateBed(req.body.pasien_id, req.body.bed)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static postDeleteDiagnosa(req, res) {
		Pasien.updateDiagnosa(req.body.pasien_id, req.body.diagnosa)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static report(req, res) {
		Pasien.report(req.body)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static postBiodata(req, res) {
		Pasien.updateBiodata(req.body)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

}

module.exports = PasienController;
