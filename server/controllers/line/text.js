const DataUser = require('../../models/data_user.js');
const DataResiden = require('../../models/data_residen.js');
const DataDpjp = require('../../models/data_dpjp.js');
const Konsul = require('../../models/konsul.js');
const Pasien = require('../../models/pasien.js');

const { infoLine } = require('../../helpers/pre_html.js');
const { hashingPassword } = require('../../configs/bcrypt.js');

class TextController {

	static info(client, event) {
		client.replyMessage(event.replyToken, {  
			type: "text",
			text: infoLine,
			wrap: true
		})
			.catch(function(error) {
				console.log('masuk====', error.originalError.response.data)
			});
	}

	static registerResiden(client, event, username) {
		client.getProfile(event.source.userId)
			.then(function(data) {
				let password = hashingPassword(username);
				DataUser.saveLine({
					line: data.userId,
					username: username,
					password: password,
					nama: data.displayName,
					status: '1',
				})
					.then(function (result) {
						client.replyMessage(event.replyToken, {  
							type: 'text',
							text: 'Register Berhasil Sebagai Residen, silahkan login dengan username dengan default password username Anda, setiap minggu pagi password akan dihapus, silahkan Masukkan Format : "password" Untuk Mendapatkan Password Baru.',
							wrap: true
						})
							.catch(function(error) {
								console.log('masuk====', error.originalError.response.data)
							});
					}).catch(function (error) {
						client.replyMessage(event.replyToken, {  
							type: 'text',
							text: error,
							wrap: true
						})
							.catch(function(error) {
								console.log('masuk====', error.originalError.response.data)
							});
					});
			})
			.catch(function(error) {
				console.log('masuk====', error.originalError.response.data)
			});
	}

	static dpjp(client, event, dpjp_id) {
		DataPasien.findByDpjp(dpjp_id)
			.then(function (result) {
				let tampil = '';
				for (let i = 0; i < result.length; i++) {
					let diagnosa = JSON.parse(result[i].diagnosa);
					let diagnosa2 = '';
					for (let j = 0; j < diagnosa.length; j++) {
						diagnosa2 += `	- ${diagnosa[j].name}\n`;
					}
					let sex = 'Tidak Diketahui';
					if (result[i].sex === 'm') {
						sex = 'Laki-Laki';
					} else if (result[i].sex === 'f') {
						sex = 'Perempuan';
					}
					tampil += `${i+1}. ${result[i].nama}
	${sex}, ${result[i].usia} thn, RM ${result[i].no_rekmedis}
	Diagnosa:
	${diagnosa2}Dpjp. ${result[i].dpjp_nama}
	Ruang ${result[i].kamar_nama} bed ${result[i].bed}
	Status: ${result[i].status_nama}
	`;
				}
				client.replyMessage(event.replyToken, {  
					type: 'text',
					text: tampil,
					wrap: true
				})
					.catch(function(error) {
						console.log('masuk====', error.originalError.response.data)
					});
			}).catch(function (error) {
				client.replyMessage(event.replyToken, {  
					type: 'text',
					text: error.response,
					wrap: true
				})
					.catch(function(error) {
						console.log('masuk====', error.originalError.response.data)
					});
			});
	}

	static residen(client, event, residen_id) {
		DataPasien.findByResiden(residen_id)
			.then(function (result) {
				let tampil = '';
				for (let i = 0; i < result.length; i++) {
					let diagnosa = JSON.parse(result[i].diagnosa);
					let diagnosa2 = '';
					for (let j = 0; j < diagnosa.length; j++) {
						diagnosa2 += `	- ${diagnosa[j].name}\n`;
					}
					let sex = 'Tidak Diketahui';
					if (result[i].sex === 'm') {
						sex = 'Laki-Laki';
					} else if (result[i].sex === 'f') {
						sex = 'Perempuan';
					}
					tampil += `${i+1}. ${result[i].nama}
	${sex}, ${result[i].usia} thn, RM ${result[i].no_rekmedis}
	Diagnosa:
	${diagnosa2}Dpjp. ${result[i].dpjp_nama}
	Ruang ${result[i].kamar_nama} bed ${result[i].bed}
	Status: ${result[i].status_nama}
	`;
				}
				client.replyMessage(event.replyToken, {  
					type: 'text',
					text: tampil,
					wrap: true
				})
					.catch(function(error) {
						console.log('masuk====', error.originalError.response.data)
					});
			}).catch(function (error) {
				client.replyMessage(event.replyToken, {  
					type: 'text',
					text: error.response,
					wrap: true
				})
					.catch(function(error) {
						console.log('masuk====', error.originalError.response.data)
					});
			});
	}

	static konsulRaberSewaktu(client, event, data) {
		Konsul.save(data)
			.then(function (result) {
				client.replyMessage(event.replyToken, {  
					type: 'text',
					text: 'Request Konsul Berhasil.',
					wrap: true
				})
					.catch(function(error) {
						console.log('masuk====', error.originalError.response.data)
					});
			})
			.catch(function (error) {
				client.replyMessage(event.replyToken, {  
					type: 'text',
					text: error.response,
					wrap: true
				})
					.catch(function(error) {
						console.log('masuk====', error.originalError.response.data)
					});
			});
	}

	static konsulSelesai(client, event, data) {
		Konsul.update(data)
			.then(function(result) {
				client.replyMessage(event.replyToken, {  
					type: 'text',
					text: 'Data Berhasil Diupdate.',
					wrap: true
				})
					.catch(function(error) {
						console.log('masuk====', error.originalError.response.data)
					});
			})
			.catch(function(error) {
				client.replyMessage(event.replyToken, {  
					type: 'text',
					text: error.response,
					wrap: true
				})
					.catch(function(error) {
						console.log('masuk====', error.originalError.response.data)
					});
			});
	}

	static konsulRaber(client, event, data) {
		Konsul.findByMr(data.no_rekmedis)
			.then(function(result) {
				Pasien.save({
					...result,
					dpjp_id: data.dpjp_id,
					diagnosa: [{ name: result.diagnosa }],
					status_id: 2
				})
					.then(function(result) {
						client.replyMessage(event.replyToken, {  
							type: 'text',
							text: 'Pasien Berhasil Dipindahkan Ke List Pasien Aktif.',
							wrap: true
						})
							.catch(function(error) {
								console.log('masuk====', error.originalError.response.data)
							});
					})
					.catch(function(error) {
						client.replyMessage(event.replyToken, {  
							type: 'text',
							text: error.response,
							wrap: true
						})
							.catch(function(error) {
								console.log('masuk====', error.originalError.response.data)
							});
					});
			})
			.catch(function(error) {
				client.replyMessage(event.replyToken, {  
					type: 'text',
					text: error.response,
					wrap: true
				})
					.catch(function(error) {
						console.log('masuk====', error.originalError.response.data)
					});
			});
	}

	static listResiden(client, event) {
		DataResiden.find()
		.then(function(result) {
			let tampil = `Nama | Kode\n`;
			for (let i = 0; i < result.length; i++) {
				tampil += `${result[i].nama} | ${result[i].id}\n`;
			}
			client.replyMessage(event.replyToken, {  
				type: 'text',
				text: tampil,
				wrap: true
			})
				.catch(function(error) {
					console.log('masuk====', error.originalError.response.data)
				});
		})
		.catch(function(error) {
			client.replyMessage(event.replyToken, {  
				type: 'text',
				text: error.response,
				wrap: true
			})
				.catch(function(error) {
					console.log('masuk====', error.originalError.response.data)
				});
		});
	}

	static listDpjp(client, event) {
		DataDpjp.find()
		.then(function(result) {
			let tampil = `Nama [Kode]\n`;
			for (let i = 0; i < result.length; i++) {
				tampil += `${result[i].nama} [${result[i].id}]\n`;
			}
			client.replyMessage(event.replyToken, {  
				type: 'text',
				text: tampil,
				wrap: true
			})
				.catch(function(error) {
					console.log('masuk====', error.originalError.response.data)
				});
		})
		.catch(function(error) {
			client.replyMessage(event.replyToken, {  
				type: 'text',
				text: error.response,
				wrap: true
			})
				.catch(function(error) {
					console.log('masuk====', error.originalError.response.data)
				});
		});
	}

	static listKonsul(client, event) {
		Konsul.find()
			.then(function(result) {
				let tampil = ``;
				for (let i = 0; i < result.length; i++) {
					tampil += `${i+1}. ${result[i].nama}, ${result[i].no_rekmedis}, ${result[i].ruang}, ${result[i].diagnosa}, ${result[i].tujuan}, ${result[i].waktu}\n`;
				}
				client.replyMessage(event.replyToken, {  
					type: 'text',
					text: tampil,
					wrap: true
				})
					.catch(function(error) {
						console.log('masuk====', error.originalError.response.data)
					});
			})
			.catch(function(error) {
				client.replyMessage(event.replyToken, {  
					type: 'text',
					text: error.response,
					wrap: true
				})
					.catch(function(error) {
						console.log('masuk====', error.originalError.response.data)
					});
			});
	}

	static formatTidakSesuai(client, event) {
		client.replyMessage(event.replyToken, {
			"type": "template",
			"altText": "This is a buttons template",
			"template": {
				 "type": "buttons",
				 "imageAspectRatio": "rectangle",
				 "imageSize": "cover",
				 "imageBackgroundColor": "#FFFFFF",
				 "title": "Error",
				 "text": "Format Tidak Sesuai",
				 "actions": [
					  {
						 "type": "postback",
						 "label": "Info",
						 "data": "action=buy&itemid=123"
					  },
				 ]
			}
		 })
			.catch(function(error) {
				console.log('masuk====', error.originalError.response.data)
			});
	}

}

module.exports = TextController;
