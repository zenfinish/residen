const { info } = require('../../helpers/pre_html.js');

class ActionController {

	static info(ctx) {
		ctx.replyWithHTML(info)
			.catch(function(error) {
				console.log('error start controller => ', error.response)
			});
	}

}

module.exports = ActionController;
