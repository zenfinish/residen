const DataUser = require('../../models/data_user.js');
const DataResiden = require('../../models/data_residen.js');
const DataDpjp = require('../../models/data_dpjp.js');
const Konsul = require('../../models/konsul.js');
const Pasien = require('../../models/pasien.js');

class UseController {

	static registerResiden({ from, reply }, username) {
		DataUser.save({
			telegram: from.id,
			username: username,
			nama: from.first_name + ' ' + from.last_name,
			status: '1',
		})
			.then(function (result) {
				reply('Register Berhasil Sebagai Residen, silahkan login dengan username dengan default password username Anda, setiap minggu pagi password akan dihapus, silahkan Masukkan Format : "password" Untuk Mendapatkan Password Baru.')
					.catch(function(error) {
						console.log('masuk===', error.response)
					});
			}).catch(function (error) {
				reply(error.response.data)
					.catch(function(error) {
						console.log('masuk===', error.response)
					});
			});
	}

	static konsulSelesai({ reply }, no_rekmedis) {
		Konsul.update({
			no_rekmedis: no_rekmedis
		})
			.then(function (result) {
				reply('Data Berhasil Diupdate.')
					.catch(function(error) {
						console.log('masuk===', error.response)
					});
			}).catch(function (error) {
				reply(error.response.data)
					.catch(function(error) {
						console.log('masuk===', error.response)
					});
			});
	}

	static konsulRaberSewaktu({ reply }, data) {
		Konsul.save({
			nama: data.nama,
			no_rekmedis: data.no_rekmedis,
			ruang: data.ruang,
			diagnosa: data.diagnosa,
			tujuan: data.tujuan,
		})
			.then(function (result) {
				reply('Request Konsul Berhasil.')
					.catch(function(error) {
						console.log('masuk===', error.response)
					});
			}).catch(function (error) {
				reply(error.response.data)
					.catch(function(error) {
						console.log('masuk===', error.response)
					});
			});
	}

	static konsulRaber({ reply }, data) {
		Konsul.findByMr(data.no_rekmedis)
			.then(function(result) {
				Pasien.save({
					...result,
					dpjp_id: data.dpjp_id,
					diagnosa: [{ name: result.diagnosa }],
					status_id: 2
				})
					.then(function(result) {
						reply('Pasien Berhasil Dipindahkan Ke List Pasien Aktif.')
							.catch(function(error) {
								console.log('masuk===', error.response)
							});
					})
					.catch(function(error) {
						reply(error.response.data)
							.catch(function(error) {
								console.log('masuk===', error.response)
							});
					});
			})
			.catch(function(error) {
				reply(error.response.data)
					.catch(function(error) {
						console.log('masuk===', error.response)
					});
			});
	}

	static listKonsul({ reply, replyWithHTML }) {
		Konsul.find()
			.then(function (result) {
				let tampil = `<pre>`;
				for (let i = 0; i < result.data.length; i++) {
					tampil += `
						${i+1}. ${result.data[i].nama},
						${result.data[i].no_rekmedis},
						${result.data[i].ruang},
						${result.data[i].diagnosa},
						${result.data[i].tujuan},
						${result.data[i].waktu}
					`;
				}
				tampil += '</pre>';
				replyWithHTML(tampil)
					.catch(function(error) {
						console.log('masuk===', error.response)
					});
			}).catch(function (error) {
				reply(error.response.data)
					.catch(function(error) {
						console.log('masuk===', error.response)
					});
			});
	}

	static registerOperator({ reply }) {
		reply('Dalam Pengembangan...')
			.catch(function(error) {
				console.log('masuk===', error.response)
			});
	}

	static listResiden({ reply, replyWithHTML }) {
		DataResiden.find()
			.then(function (result) {
				let tampil = `<pre>Nama | Kode`;
				for (let i = 0; i < result.data.length; i++) {
					tampil += `${result.data[i].nama} | ${result.data[i].id}`;
				}
				tampil += '</pre>';
				replyWithHTML(tampil)
					.catch(function(error) {
						console.log('masuk===', error.response)
					});
			}).catch(function (error) {
				reply(error.response.data)
					.catch(function(error) {
						console.log('masuk===', error.response)
					});
			});
	}

	static listDpjp({ reply, replyWithHTML }) {
		DataDpjp.find()
			.then(function (result) {
				let tampil = `<pre>Nama | Kode`;
				for (let i = 0; i < result.data.length; i++) {
					tampil += `${result.data[i].nama} | ${result.data[i].id}`;
				}
				tampil += '</pre>';
				replyWithHTML(tampil)
					.catch(function(error) {
						console.log('masuk===', error.response)
					});
			}).catch(function (error) {
				reply(error.response.data)
					.catch(function(error) {
						console.log('masuk===', error.response)
					});
			});
	}

	static residen({ from, reply, replyWithHTML }, next) {
		console.log('masuk=====');
	}

	static dpjp({ from, reply, replyWithHTML }, next) {
		console.log('masuk=====');
	}

	static salahInput({ from, reply, replyWithHTML }, next) {
		console.log('masuk=====');
	}

}

module.exports = UseController;
