const { info } = require('../../helpers/pre_html.js');

class StartController {

	static start1(ctx) {
		ctx.replyWithHTML(info)
			.catch(function(error) {
				console.log('error start controller => ', error.response)
			});
	}

}

module.exports = StartController;
