const Pasien = require('../models/pasien.js');

class ReportController {

	static post(req, res) {
		Pasien.report(req.body)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static postPasienPulang(req, res) {
		Pasien.findPasienPulang(req.body)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

}

module.exports = ReportController;
