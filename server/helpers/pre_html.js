const info = `<pre>1. Format:	"register/residen/:username/:passwordBot"
Untuk Mendaftar Sebagai Residen.

2. Format: "register/operator/:username/:passwordBot"
Untuk Mendaftar Sebagai Operator.

3. Format: "dpjp/:kode_dpjp/:passwordBot"
Untuk Melihat Data Pasien Berdasarkan DPJP.

4. Format: "residen/:kode_residen/:passwordBot"
Untuk Melihat Data Pasien Berdasarkan Residen.

5. Format: "konsul/nama/:rm/ruang/diagnosa/:alasan"
Untuk Request Konsul.

6. Format: "konsul/:rm/selesai/:passwordBot"
Untuk Menjawab Konsul.

7. Format: "konsul/:rm/raber/:kode_dpjp/:passwordBot"
Untuk Memasukkan Pasien Konsul Ke List Pasien Aktif.

8. Format: "list/residen/:passwordBot"
Untuk Melihat Seluruh Data Residen.

9. Format: "list/dpjp/:passwordBot"
Untuk Melihat Seluruh Data DPJP.

10. Format: "list/konsul/:passwordBot"
Untuk Minta List Pasien Konsul.</pre>`;

const infoLine = `
1. Format:	"register/residen/:username/:passwordBot"
Untuk Mendaftar Sebagai Residen.

2. Format: "register/operator/:username/:passwordBot"
Untuk Mendaftar Sebagai Operator.

3. Format: "dpjp/:kode_dpjp/:passwordBot"
Untuk Melihat Data Pasien Berdasarkan DPJP.

4. Format: "residen/:kode_residen/:passwordBot"
Untuk Melihat Data Pasien Berdasarkan Residen.

5. Format: "konsul/nama/:rm/ruang/diagnosa/:alasan"
Untuk Request Konsul.

6. Format: "konsul/:rm/selesai/:passwordBot"
Untuk Menjawab Konsul.

7. Format: "konsul/:rm/raber/:kode_dpjp/:passwordBot"
Untuk Memasukkan Pasien Konsul Ke List Pasien Aktif.

8. Format: "list/residen/:passwordBot"
Untuk Melihat Seluruh Data Residen.

9. Format: "list/dpjp/:passwordBot"
Untuk Melihat Seluruh Data DPJP.

10. Format: "list/konsul/:passwordBot"
Untuk Minta List Pasien Konsul.
`;

module.exports = { info, infoLine };