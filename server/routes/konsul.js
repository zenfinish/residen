const express = require('express');
const KonsulController = require('../controllers/konsul.js');
const router = express.Router();

router.post('/', KonsulController.post);
router.get('/', KonsulController.get);
router.put('/', KonsulController.put);
router.post('/pindah', KonsulController.postPindah);

module.exports = router;