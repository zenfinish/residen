const express = require('express');
const PasienController = require('../controllers/pasien.js');
const { isUser } = require('../middlewares/is-user.js');
const router = express.Router();

router.get('/', PasienController.get);
router.get('/dpjp/:dpjp_id', isUser, PasienController.getDpjp);
router.get('/status/:status_id', isUser, PasienController.getStatus);
router.get('/jejaring', PasienController.getJejaring);
router.get('/nonjejaring', PasienController.getNonjejaring);

router.put('/kamar/:pasien_id/:kamar_id', isUser, PasienController.putKamar);
router.put('/residen/:pasien_id/:residen_id', isUser, PasienController.putResiden);
router.put('/status/:pasien_id/:status_id', isUser, PasienController.putStatus);

router.post('/', isUser, PasienController.post);
router.post('/checkout', isUser, PasienController.postCheckout);
router.post('/keterangan', isUser, PasienController.postKeterangan);
router.post('/bed', isUser, PasienController.postBed);
router.post('/delete/diagnosa', isUser, PasienController.postDeleteDiagnosa);
router.post('/biodata', PasienController.postBiodata);

module.exports = router;