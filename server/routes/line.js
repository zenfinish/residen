const express = require('express');
const router = express.Router();
const { Client } = require('@line/bot-sdk');

const client = new Client({
	channelAccessToken: process.env.LINE_TOKEN,
	channelSecret: process.env.LINE_SECRET
});
const TextController = require('../controllers/line/text.js');

router.post('/', (req, res) => {
	let event = req.body.events[0];
	if (event.type === 'message') {
		if (event.message.type !== 'text') {
			client.replyMessage(event.replyToken, {
				type: 'text',
				text: 'Tipe Data Ini Belum Tersedia, Coming soon...'
			});
		} else {
			let text = event.message.text.toLowerCase();
			let text1 = text.split('/');
			if (text1[0] === 'info') {
				TextController.info(client, event);
			} else if (text1[0] === 'register' && text1[1] === 'residen' && text1[2] !== '' && text1[2] !== undefined && text1[3] === 'jpd88') {
				TextController.registerResiden(client, event, text1[2]);
			}
			else if (text1[0] === 'konsul' && text1[1] !== '' && text1[1] !== undefined && text1[2] === 'selesai' && text1[3] === 'jpd88') {
				TextController.konsulSelesai(client, event, {
					no_rekmedis: text1[1],
				});
			}
			else if (text1[0] === 'konsul' && text1[1] !== '' && text1[1] !== undefined && text1[2] !== '' && text1[2] !== undefined && text1[3] !== '' && text1[3] !== undefined && text1[4] !== '' && text1[4] !== undefined && (text1[5] === 'raber' || text1[5] === 'sewaktu')) {
				TextController.konsulRaberSewaktu(client, event, {
					no_rekmedis: text1[2],
					nama: text1[1],
					ruang: text1[3],
					diagnosa: text1[4],
					tujuan: text1[5],
				});
			}
			else if (text1[0] === 'konsul' && text1[1] !== '' && text1[1] !== undefined && text1[2] === 'raber' && text1[3] !== '' && text1[3] !== undefined && text1[4] === 'jpd88') {
				TextController.konsulRaber(client, event, {
					no_rekmedis: text1[1],
					dpjp_id: text1[3],
				});
			}
			else if (text1[0] === 'list' && text1[1] === 'konsul' && text1[2] === 'jpd88') {
				TextController.listKonsul(client, event);
			}
			else if (text1[0] === 'register' && text1[1] === 'operator' && text1[2] !== '' && text1[2] !== undefined && text1[3] === 'ppsd') {

			}
			else if (text1[0] === 'list' && text1[1] === 'residen' && text1[2] === 'jpd88') {
				TextController.listResiden(client, event);
			}
			else if (text1[0] === 'list' && text1[1] === 'dpjp' && text1[2] === 'jpd88') {
				TextController.listDpjp(client, event);
			}
			else if (text1[0] === 'residen' && text1[1] !== '' && text1[1] !== undefined && text1[2] === 'jpd88') {
				TextController.residen(client, event, text1[1]);
			}
			else if (text1[0] === 'dpjp' && text1[1] !== '' && text1[1] !== undefined && text1[2] === 'jpd88') {
				TextController.dpjp(client, event, text1[1]);
			}
			else {
				TextController.formatTidakSesuai(client, event);
			}
		}
	} else if (event.type === 'postback') {
		TextController.info(client, event);
	}
});

module.exports = router;