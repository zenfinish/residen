const express = require('express');
const ReportController = require('../controllers/report.js');
const router = express.Router();

router.post('/', ReportController.post);
router.post('/pasien-pulang', ReportController.postPasienPulang);

module.exports = router;