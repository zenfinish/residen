const UseController = require('../../controllers/telegram/use.js');

function useRoute({ update, from, reply, replyWithHTML }, next) {
	let text = update.message.text.split('/');
	if (text[0] === 'register' && text[1] === 'residen' && text[2] !== '' && text[2] !== undefined && text[3] === 'jpd88') {
		UseController.registerResiden({ from, reply }, text[2]);
	}
	else if (text[0] === 'konsul' && text[1] !== '' && text[1] !== undefined && text[2] === 'selesai' && text[3] === 'jpd88') {
		UseController.konsulSelesai({ reply }, text[1]);
	}
	else if (text[0] === 'konsul' && text[1] !== '' && text[1] !== undefined && text[2] !== '' && text[2] !== undefined && text[3] !== '' && text[3] !== undefined && text[4] !== '' && text[4] !== undefined && (text[5] === 'raber' || text[5] === 'sewaktu')) {
		UseController.konsulRaberSewaktu({ reply }, {
			nama: text[1],
			no_rekmedis: text[2],
			ruang: text[3],
			diagnosa: text[4],
			tujuan: text[5],
		});
	}
	else if (text[0] === 'konsul' && text[1] !== '' && text[1] !== undefined && text[2] === 'raber' && text[3] !== '' && text[3] !== undefined && text[4] === 'jpd88') {
		UseController.konsulRaber({ reply }, {
			no_rekmedis: text[1],
			dpjp_id: text[3],
		});
	}
	else if (text[0] === 'list' && text[1] === 'konsul' && text[2] === 'jpd88') {
		UseController.listKonsul({ reply, replyWithHTML });
	}
	else if (text[0] === 'register' && text[1] === 'operator' && text[2] !== '' && text[2] !== undefined && text[3] === 'ppsd') {
		UseController.registerOperator({ reply });
	}
	else if (text[0] === 'list' && text[1] === 'residen' && text[2] === 'jpd88') {
		UseController.listResiden({ reply, replyWithHTML });
	}
	else if (text[0] === 'list' && text[1] === 'dpjp' && text[2] === 'jpd88') {
		UseController.listDpjp({ reply, replyWithHTML });
	}
	else if (text[0] === 'residen' && text[1] !== '' && text[1] !== undefined && text[2] === 'jpd88') {
		UseController.residen({ from, reply, replyWithHTML }, next);
	}
	else if (text[0] === 'dpjp' && text[1] !== '' && text[1] !== undefined && text[2] === 'jpd88') {
		UseController.dpjp({ from, reply, replyWithHTML }, next);
	}
	else {
		UseController.salahInput({ from, reply, replyWithHTML }, next);
	}
}

module.exports = useRoute;
