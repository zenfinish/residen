const express = require('express');
const ResidenController = require('../controllers/residen.js');
const { isUser } = require('../middlewares/is-user.js');
const router = express.Router();

router.get('/', ResidenController.get);
router.get('/aktif/:residen_id', ResidenController.getAktif);
router.get('/pasien', isUser, ResidenController.getPasien);

module.exports = router;