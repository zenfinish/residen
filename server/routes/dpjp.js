const express = require('express');
const DpjpController = require('../controllers/dpjp.js');
const { isUser } = require('../middlewares/is-user.js');
const router = express.Router();

router.get('/', DpjpController.get);
router.get('/aktif/:dpjp_id', DpjpController.getAktif);

module.exports = router;