const express = require('express');
const KamarController = require('../controllers/kamar.js');
const { isUser } = require('../middlewares/is-user.js');
const router = express.Router();

router.get('/', isUser, KamarController.get);

module.exports = router;