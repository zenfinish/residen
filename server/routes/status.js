const express = require('express');
const StatusController = require('../controllers/status.js');
const router = express.Router();

router.get('/', StatusController.get);

module.exports = router;