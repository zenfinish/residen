const express = require('express');
const UserController = require('../controllers/user.js');
const { isUser } = require('../middlewares/is-user.js');
const router = express.Router();

router.post('/register/', UserController.postRegister);
router.post('/login/', UserController.postLogin);
router.get('/cek-token/', isUser, UserController.getCekToken);

module.exports = router;