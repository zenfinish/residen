const express = require('express');
const cors = require('cors');
const Telegraf = require('telegraf');
const http = require('http');
const https = require('https');

require('dotenv').config();

const fs = require('fs');
const privateKey  = fs.readFileSync(`${process.env.PRIVKEY}`, 'utf8');
const certificate = fs.readFileSync(`${process.env.FULLCHAIN}`, 'utf8');
const credentials = { key: privateKey, cert: certificate };

const app = express();
// const bot = new Telegraf(process.env.TELEGRAM_TOKEN);

app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

const userRoute = require('./routes/user.js');
const dpjpRoute = require('./routes/dpjp.js');
const kamarRoute = require('./routes/kamar.js');
const pasienRoute = require('./routes/pasien.js');
const residenRoute = require('./routes/residen.js');
const statusRoute = require('./routes/status.js');
const konsulRoute = require('./routes/konsul.js');
const lineRoute = require('./routes/line.js');
const reportRoute = require('./routes/report.js');

// const telegramStartRoute = require('./routes/telegram/start.js');
// const telegramUseRoute = require('./routes/telegram/use.js');
// const telegramActionRoute = require('./routes/telegram/action.js');

app.use('/user', userRoute);
app.use('/dpjp', dpjpRoute);
app.use('/kamar', kamarRoute);
app.use('/pasien', pasienRoute);
app.use('/residen', residenRoute);
app.use('/status', statusRoute);
app.use('/konsul', konsulRoute);
app.use('/report', reportRoute);
app.use('/webhook', lineRoute);

app.use('/', function(req, res) {
	res.json(`!@#$%%^&*`);
});

// bot.start(telegramStartRoute);
// bot.action(telegramActionRoute);
// bot.use(telegramUseRoute);

// bot.launch()
// 	.catch((error) => {
// 		console.log('error launch =>', error)
// 	});

const httpServer = http.createServer(app);
const httpsServer = https.createServer(credentials, app);

httpServer.listen(process.env.PORT_HTTP, function() {
	console.log('PORT HTTP: '+process.env.PORT_HTTP)
});

httpsServer.listen(process.env.PORT_HTTPS, function() {
	console.log('PORT HTTPS: '+process.env.PORT_HTTPS)
});

/*

Pilih semua error
kamar tambahin Igd 1 , Igd 2,
kamar default igd 1
usia ganti tgl lahir
status ppds dikasih tombol
dipisahin: konsul, list pasien sama residen , input pasien dan list pasien

doktermuda doktermudajpd
admin cardioisfun

*/
