import React, { Component } from 'react';
import { Segment, Button, Select, Input } from 'semantic-ui-react';
import 'react-semantic-ui-datepickers/dist/react-semantic-ui-datepickers.css';
import api from 'configs/api.js';
import Menu from 'components/Menu.jsx';

class FormReport extends Component {

	state = {

		listDpjp: [],
		listKamar: [],
		listResiden: [],
		listStatus: [],

		residen: '',
		kamar: '',
		dpjp: '',
		status: '',
		jejaring: '',

		tgl_dari: '',
		tgl_sampai: '',
		loadingPulang: false,

		data: [],
		dataPulang: [],

		loadingDaftar: false,
		messageDaftar: 'none',
		messageDaftarText: '',

		loadingListPasien: false,
		loadingListResiden: false,
		loadingListKonsul: false,

		messagePulang: 'none',
		messagePulangText: '',

	}
	
	componentDidMount() {
		
		api.get(`/user/cek-token`)
		.then(result => {
			this.setState({ token: true });
		})
		.catch(error => {
			this.setState({ token: false }, () => {
				this.props.history.push('/');
			});
		});
		
		api.get(`/dpjp`)
		.then(result => {
			let listDpjp = [];
			for (let i = 0; i < result.data.length; i++) {
				listDpjp.push({ key: result.data[i].id, text: result.data[i].nama, value: result.data[i].id });
			}
			this.setState({ listDpjp: listDpjp });
		})
		.catch(error => {
			console.log(error.response);
		});

		api.get(`/kamar`)
		.then(result => {
			let listKamar = [];
			for (let i = 0; i < result.data.length; i++) {
				listKamar.push({ key: result.data[i].id, text: result.data[i].nama, value: result.data[i].id });
			}
			this.setState({ listKamar: listKamar });
		})
		.catch(error => {
			console.log(error.response);
		});

		api.get(`/residen`)
		.then(result => {
			let listResiden = [];
			for (let i = 0; i < result.data.length; i++) {
				listResiden.push({ key: result.data[i].id, text: result.data[i].nama, value: result.data[i].id });
			}
			this.setState({ listResiden: listResiden });
		})
		.catch(error => {
			console.log(error.response);
		});

		api.get(`/status`)
		.then(result => {
			let listStatus = [];
			for (let i = 0; i < result.data.length; i++) {
				listStatus.push({ key: result.data[i].id, text: result.data[i].nama, value: result.data[i].id });
			}
			this.setState({ listStatus: listStatus });
		})
		.catch(error => {
			console.log(error.response);
		});
	}

	logout = () => {
		localStorage.removeItem('token');
		localStorage.removeItem('username');
		this.props.history.push('/');
		window.location.reload();
	}
	
	render() {
		return (
			<Segment basic>
				<Menu />
				<Segment raised>
					<h3>Pasien Aktif</h3>
					<Select
						options={[ { key: '', text: '- Semua -', value: '' }, ...this.state.listResiden]}
						placeholder='Residen'
						onChange={(e, { value }) => {
							this.setState({ residen: value })
						}}
						value={this.state.residen}
					/>
					<Select
						options={[ { key: '', text: '- Semua -', value: '' }, ...this.state.listKamar]}
						placeholder='Kamar'
						onChange={(e, { value }) => {
							this.setState({ kamar: value })
						}}
						value={this.state.kamar}
					/>
					<Select
						options={[ { key: '', text: '- Semua -', value: '' }, ...this.state.listDpjp]}
						placeholder='DPJP'
						onChange={(e, { value }) => {
							this.setState({ dpjp: value })
						}}
						value={this.state.dpjp}
					/>
					<Select
						options={[ { key: '', text: '- Semua -', value: '' }, ...this.state.listStatus]}
						placeholder='Status'
						onChange={(e, { value }) => {
							this.setState({ status: value })
						}}
						value={this.state.status}
					/>
					<Select
						options={[
							{ key: '', text: '- Semua -', value: '' },
							{ key: 'jejaring', text: 'Jejaring', value: 'jejaring' },
							{ key: 'nonjejaring', text: 'Non Jejaring', value: 'nonjejaring' }
						]}
						placeholder='Jejaring'
						onChange={(e, { value }) => {
							this.setState({ jejaring: value })
						}}
						value={this.state.jejaring}
					/>
					<Button onClick={() => {
						api.post(`/report`, {
							residen: this.state.residen,
							kamar: this.state.kamar,
							dpjp: this.state.dpjp,
							status: this.state.status,
							jejaring: this.state.jejaring
						})
						.then(result => {
							this.setState({ data: result.data });
						})
						.catch(error => {
							console.log(error.response);
						});
					}}>Cari</Button><br /><br />
					{
						this.state.data.map((item, index) => (
							<span key={index}>
								{index + 1}. {item.kamar_nama}, {item.bed}<br />
								{item.nama}, {item.sex}, {item.usia}th, {item.no_rekmedis}<br />
								Dx: {JSON.parse(item.diagnosa).map((item2, index2) => (
									<span key={index2}>{item2.name}, </span>
								))}<br />
								Status: {item.status_nama}<br />
								Konsulen: {item.dpjp_nama}<br />
								Keterangan: {item.keterangan}<br />
								<br />
							</span>
						))
					}
				</Segment>
				<Segment raised>
					<h3>Pasien Pulang</h3>
					<Input type="date" label="Dari Tgl" onChange={(e, { value }) => {
						this.setState({ tgl_dari: value });
					}} />
					<Input type="date" label="Sampai Tgl" onChange={(e, { value }) => {
						this.setState({ tgl_sampai: value });
					}} />
					<Button onClick={() => {
						this.setState({
							loadingPulang: true,
						}, () => {
							api.post(`/report/pasien-pulang`, {
								tgl_dari: this.state.tgl_dari,
								tgl_sampai: this.state.tgl_sampai,
							})
							.then(result => {
								this.setState({ dataPulang: result.data });
								this.setState({ loadingPulang: false });
							})
							.catch(error => {
								console.log(error.response);
								this.setState({ loadingPulang: false });
							});
						});
					}} loading={this.state.loadingPulang}>Cari</Button><br /><br />
					{
						this.state.dataPulang.length > 0 ?
						<table style={{ whiteSpace: 'nowrap', borderCollapse: 'collapse' }} border="1" cellPadding={5}>
							<thead>
								<tr>
									<th>No.</th>
									<th>Tgl Checkout</th>
									<th>No. Rekmedis</th>
									<th>Nama Pasien</th>
									<th>Dpjp</th>
									<th>Residen</th>
									<th>Kamar</th>
									<th>Status</th>
									<th>Diagnosa</th>
								</tr>
							</thead>
							<tbody>
								{
									this.state.dataPulang.map((row, i) => (
										<tr key={i}>
											<td>{i+1}</td>
											<td>{row.tgl_checkout}</td>
											<td>{row.no_rekmedis}</td>
											<td>{row.nama}</td>
											<td>{row.dpjp_nama}</td>
											<td>{row.residen_nama}</td>
											<td>{row.kamar_nama}</td>
											<td>{row.status_nama}</td>
											<td>
												{
													JSON.parse(row.diagnosa).map((item2, index2) => (
														<span key={index2}>{item2.name}, </span>
													))
												}
											</td>
										</tr>
									))
								}
							</tbody>
						</table> : null
					}
				</Segment>
			</Segment>
		)
	}
}

export default FormReport
