import React, { Component } from 'react';
import { Button, Modal, Header, Input, Icon, Select } from 'semantic-ui-react';
import api from 'configs/api.js';

class EditBiodata extends Component {

  state = {
    id: '',
    nama: '',
    no_rekmedis: '',
    tgl_lahir: '',
    sex: '',
  }

  componentDidMount() {
    this.setState({
      id: this.props.data.id,
      nama: this.props.data.nama,
      no_rekmedis: this.props.data.no_rekmedis,
      tgl_lahir: this.props.data.tgl_lahir,
      sex: this.props.data.sex
    });
  }
  
  render() {
		return (
      <Modal open={true} onClose={() => { this.props.close(); }} dimmer="blurring">
        <Modal.Header>Detail Pasien</Modal.Header>
        <Modal.Content scrolling>
          <Modal.Description>
            <Header>Nama</Header>
            <Input
              placeholder='Nama'
              onChange={(e) => {
              	this.setState({ nama: e.target.value })
              }}
              value={this.state.nama}
              fluid
            />
            <Header>No. Rekmedis</Header>
            <Input
              placeholder='No. Rekmedis'
              onChange={(e) => {
              	this.setState({ no_rekmedis: e.target.value })
              }}
              value={this.state.no_rekmedis}
              fluid
            />
            <Header>Tgl. Lahir</Header>
            <Input
              onChange={(e, data) => {
                this.setState({ tgl_lahir: data.value })
              }}
              value={this.state.tgl_lahir}
              type="date"
            />
            <Header>Sex</Header>
            <Select
              options={[
                { key: 'm', text: 'Male', value: 'm' },
                { key: 'f', text: 'Female', value: 'f' },
                { key: 'o', text: 'Other', value: 'o' },
              ]}
              placeholder='Sex'
              onChange={(e, { value }) => {
                this.setState({ sex: value })
              }}
              value={this.state.sex}
              fluid
            />
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          <Button color='red' onClick={() => {
            api.post(`/pasien/biodata`, {
              no_rekmedis: this.state.no_rekmedis,
              nama: this.state.nama,
              tgl_lahir: this.state.tgl_lahir,
              sex: this.state.sex,
              pasien_id: this.state.id,
            })
            .then(result => {
              this.props.close();
            })
            .catch(error => {
              console.log(error.response)
            });
          }}>
            <Icon name='save' /> Update
          </Button>
        </Modal.Actions>
      </Modal>
		)
	}
}

export default EditBiodata
