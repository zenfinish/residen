import React, { Component, Fragment } from 'react'
import { Button, Form, Grid, Header, Segment, Modal, Message } from 'semantic-ui-react';
import api from 'configs/api.js';
import Footer from 'components/Footer.jsx';

class Login extends Component {

	state = {
		username: '',
		password: '',

		openAlert: false,
		textAlert: '',
	}

	login = (e) => {
		e.preventDefault();
		api.post(`/user/login`, {
			...this.state
		})
		.then(result => {
			localStorage.setItem('token', result.data.token);
			localStorage.setItem('username', result.data.username);
			this.props.history.push('/main');
			window.location.reload();
		})
		.catch(error => {
			this.openAlert(JSON.stringify(error.response.data));
		});
	}

	openAlert = (text) => {
		this.setState({ openAlert: true, textAlert: text });
	}

	closeAlert = (text) => {
		this.setState({ openAlert: false, textAlert: '' });
	}
	
	render() {
		return (
			<Fragment>
				<Footer />
				<Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
					<Grid.Column style={{ maxWidth: 450 }}>
						<Header as='h2' color='teal'>Log-in Admin</Header>
						<Form size='large' onSubmit={this.login}>
							<Segment stacked>
								<Form.Input
									fluid icon='user'
									iconPosition='left'
									placeholder='Username'
									autoComplete="off"
									onChange={(e) => {this.setState({ username: e.target.value })}}
								/>
								<Form.Input
									fluid
									icon='lock'
									iconPosition='left'
									placeholder='Password'
									type='password'
									autoComplete="off"
									onChange={(e) => {this.setState({ password: e.target.value })}}
								/>
								<Button color='teal' fluid size='large'>Login</Button>
							</Segment>
						</Form>
						<Modal basic size='small' open={this.state.openAlert} onClose={() => { this.closeAlert() }}>
							<Modal.Content>
								<Message negative>
									<Message.Header>Error!</Message.Header>
									<p>{this.state.textAlert}</p>
								</Message>
							</Modal.Content>
						</Modal>
					</Grid.Column>
				</Grid>
			</Fragment>
		)
	}

}

export default Login;
