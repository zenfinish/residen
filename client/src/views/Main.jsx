import React, { Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Grid, Card, CardActionArea, CardContent, Button, Container } from '@material-ui/core';
import { Redirect, Link } from 'react-router-dom';

import StorageIcon from '@material-ui/icons/Storage';
import BarChartIcon from '@material-ui/icons/BarChart';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import LocalHotelIcon from '@material-ui/icons/LocalHotel';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';

import api from 'configs/api.js';
import Footer from 'components/Footer.jsx';

const styles = theme => ({
	card: {
		maxWidth: 345,
		borderRadius: 15,
	},
});

class Main extends React.Component {

	state = {
		isAuth: false,
	}
	
	UNSAFE_componentWillMount() {
		api.get(`/user/cektoken`, {
			headers: { token: localStorage.getItem('token') }
		})
		.catch(error => {
			this.setState({ isAuth: true });
		});
	}
	
	render() {
		const { classes } = this.props;
		return (
			<Fragment>
				<Footer />
				<Container>
					<Grid
						container
						alignItems="center"
						justify="center"
						style={{ minHeight: '100vh', padding: 80 }}
						spacing={3}
					>

						<Grid item sm={4}>
							<Card className={classes.card} elevation={0}>
								<CardActionArea component={Link} to="/input">
									<div align="center"><StorageIcon style={{ fontSize: 140, color: "red" }} /></div>
									<CardContent>
										<div align="center">Input Pasien</div>
									</CardContent>
								</CardActionArea>
							</Card>
						</Grid>

						{
							localStorage.getItem('username') === 'admin' ?
							<Fragment>
								<Grid item sm={4}>
									<Card className={classes.card} elevation={0}>
										<CardActionArea component={Link} to="/residen">
											<div align="center"><AccountBalanceIcon style={{ fontSize: 140, color: "#6495ED" }} /></div>
											<CardContent>
												<div align="center">Residen</div>
											</CardContent>
										</CardActionArea>
									</Card>  
								</Grid>

								<Grid item sm={4}>
									<Card className={classes.card} elevation={0}>
										<CardActionArea component={Link} to="/konsul">
											<div align="center"><LocalHotelIcon style={{ fontSize: 140, color: "green" }} /></div>
											<CardContent>
												<div align="center">Konsul</div>
											</CardContent>
										</CardActionArea>
									</Card>  
								</Grid>

							</Fragment> : null
						}

						<Grid item sm={4}>
							<Card className={classes.card} elevation={0}>
								<CardActionArea component={Link} to="/report">
									<div align="center"><BarChartIcon style={{ fontSize: 140, color: "blue" }} /></div>
									<CardContent>
										<div align="center">Report</div>
									</CardContent>
								</CardActionArea>
							</Card>  
						</Grid>
						
						<Grid item sm={4}>
							<Card className={classes.card} elevation={0}>
								<CardActionArea component={Button} onClick={() => {
									localStorage.removeItem('token');
									this.setState({ isAuth: true });
								}}>
									<div align="center"><ExitToAppIcon style={{ fontSize: 140, color: "black" }} /></div>
									<CardContent>
										<div align="center">Logout</div>
									</CardContent>
								</CardActionArea>
							</Card>  
						</Grid>

					</Grid>
					{this.state.isAuth ? <Redirect to='/'  /> : ''}
				</Container>
			</Fragment>
		)
	}

};

export default withStyles(styles)(Main);
