import React, { Component } from 'react';
import { Segment, Table, Button } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import api from 'configs/api.js';

class FormKonsul extends Component {

	state = {
		nama: '',
		no_rekmedis: '',
		usia: '',
		diagnosa: [],
		sex: '',
		dpjp: '',
		kamar: '',
		status: '',
		bed: '',

		listDpjp: [],
		listKamar: [],
		listPasien: [],
		listResiden: [],
		listResidenGroup: [],
		listKonsul: [],

		loadingDaftar: false,
		messageDaftar: 'none',
		messageDaftarText: '',

		loadingListPasien: false,
		loadingListResiden: false,
		loadingListKonsul: false,

		messagePulang: 'none',
		messagePulangText: '',

		openModal: false,

		aktif: {
			nama: '',
			no_rekmedis: '',
			diagnosa: '[]',
			usia: '',
			sex: '',
			dpjp_nama: '',
			residen_nama: '',
		}
	}
	
	componentDidMount() {
		
		api.get(`/user/cek-token`)
		.then(result => {
			this.setState({ token: true });
		})
		.catch(error => {
			this.setState({ token: false }, () => {
				this.props.history.push('/');
			});
		});
		
		this.fetchKonsul();
	}

	fetchKonsul = () => {
		this.setState({
			loadingListKonsul: true
		}, () => {
			api.get(`/konsul`)
			.then(result => {
				this.setState({ listKonsul: result.data, loadingListKonsul: false });
			})
			.catch(error => {
				console.log(error.response);
				this.setState({ loadingListKonsul: false });
			});
		});
	}

	logout = () => {
		localStorage.removeItem('token');
		localStorage.removeItem('username');
		this.props.history.push('/');
		window.location.reload();
	}
	
	render() {
		return (
			<Segment basic>
				<Button onClick={this.logout}>Logout</Button>
				<Button primary as={Link} to="/input" >Input Pasien</Button>
				<Button primary as={Link} to="/residen" >Residen</Button>
				<Button primary as={Link} to="/konsul" >Konsul</Button>
				<Button primary as={Link} to="/report" >Report</Button>
				<Segment raised loading={this.state.loadingListKonsul}>
					<h3>Konsul</h3>
					<Table color="yellow">
						<Table.Header>
							<Table.Row>
								<Table.HeaderCell>No.</Table.HeaderCell>
								<Table.HeaderCell>Waktu</Table.HeaderCell>
								<Table.HeaderCell>No. RM</Table.HeaderCell>
								<Table.HeaderCell>Nama</Table.HeaderCell>
								<Table.HeaderCell>Ruang</Table.HeaderCell>
								<Table.HeaderCell>Diagnosa</Table.HeaderCell>
								<Table.HeaderCell>Tujuan</Table.HeaderCell>
								<Table.HeaderCell>Status PPDS</Table.HeaderCell>
							</Table.Row>
						</Table.Header>
						<Table.Body>
							{this.state.listKonsul.map((item, index) => (
								<Table.Row key={index}>
									<Table.Cell>{index+1}</Table.Cell>
									<Table.Cell>{item.waktu}</Table.Cell>
									<Table.Cell>{item.no_rekmedis}</Table.Cell>
									<Table.Cell>{item.nama}</Table.Cell>
									<Table.Cell>{item.ruang}</Table.Cell>
									<Table.Cell>{item.diagnosa}</Table.Cell>
									<Table.Cell>{item.tujuan}</Table.Cell>
									<Table.Cell>
										<Button content='Belum Dijawab' secondary />
									</Table.Cell>
								</Table.Row>
							))}
						</Table.Body>
					</Table>
				</Segment>
			</Segment>
		)
	}
}

export default FormKonsul
