import React, { Component, Fragment } from 'react';
import { Segment, Table, Button, Label, Message, Popup, Input, Dropdown, List, Icon, Modal } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import api from 'configs/api.js';
import EditBiodata from './EditBiodata.jsx';
import Footer from '../components/Footer.jsx';

class FormResiden extends Component {

	state = {
		nama: '',
		no_rekmedis: '',
		usia: '',
		diagnosa: [],
		sex: '',
		dpjp: '',
		kamar: '',
		status: '',
		bed: '',

		listDpjp: [],
		listKamar: [],
		listPasien: [],
		listResiden: [],
		listStatus: [],
		listResidenGroup: [],

		loadingDaftar: false,
		messageDaftar: 'none',
		messageDaftarText: '',

		loadingListPasien: false,
		loadingListResiden: false,
		loadingListKonsul: false,

		messagePulang: 'none',
		messagePulangText: '',

		diagnosa_aktif: '',

		openEditBiodata: false,

		aktif: {},

		openAlert: false,
		opsiPulang: '',
		pasien_id: '',
		residen_id: '',
	}
	
	componentDidMount() {
		
		api.get(`/user/cek-token`)
		.then(result => {
			this.setState({ token: true });
		})
		.catch(error => {
			this.setState({ token: false }, () => {
				this.props.history.push('/');
			});
		});
		
		api.get(`/dpjp`)
		.then(result => {
			let listDpjp = [];
			for (let i = 0; i < result.data.length; i++) {
				listDpjp.push({ key: result.data[i].id, text: result.data[i].nama, value: result.data[i].id });
			}
			this.setState({ listDpjp: listDpjp });
		})
		.catch(error => {
			console.log(error.response);
		});

		api.get(`/kamar`)
		.then(result => {
			let listKamar = [];
			for (let i = 0; i < result.data.length; i++) {
				listKamar.push({ key: result.data[i].id, text: result.data[i].nama, value: result.data[i].id });
			}
			this.setState({ listKamar: listKamar });
		})
		.catch(error => {
			console.log(error.response);
		});

		api.get(`/residen`)
		.then(result => {
			let listResiden = [];
			for (let i = 0; i < result.data.length; i++) {
				listResiden.push({ key: result.data[i].id, text: result.data[i].nama, value: result.data[i].id });
			}
			this.setState({ listResiden: listResiden });
		})
		.catch(error => {
			console.log(error.response);
		});

		api.get(`/status`)
		.then(result => {
			let listStatus = [];
			for (let i = 0; i < result.data.length; i++) {
				listStatus.push({ key: result.data[i].id, text: result.data[i].nama, value: result.data[i].id });
			}
			this.setState({ listStatus: listStatus });
		})
		.catch(error => {
			console.log(error.response);
		});

		this.fetchResiden();
		this.fetchPasien();
		this.fetchKonsul();
	}

	fetchPasien = () => {
		this.setState({
			listPasien: [],
			loadingListPasien: true
		}, () => {
			api.get(`/pasien`)
			.then(async result => {
				for (let i = 0; i < result.data.length; i++) {
					await this.setState({ ['selectKamar'+result.data[i].id]: result.data[i].kamar_id })
					await this.setState({ ['selectResiden'+result.data[i].id]: result.data[i].residen_id })
					await this.setState({ ['selectStatus'+result.data[i].id]: result.data[i].status_id })
					await this.setState({ ['keterangan'+result.data[i].id]: result.data[i].keterangan })
					await this.setState({ ['bed'+result.data[i].id]: result.data[i].bed })
				}
				this.setState({ listPasien: result.data, loadingListPasien: false });
			})
			.catch(error => {
				console.log(error.response);
				this.setState({ loadingListPasien: false });
			});
		});
	}

	fetchResiden = () => {
		this.setState({
			loadingListResiden: true
		}, () => {
			api.get(`/residen/pasien`)
			.then(result => {
				this.setState({ listResidenGroup: result.data, loadingListResiden: false });
			})
			.catch(error => {
				console.log(error.response);
				this.setState({ loadingListResiden: false });
			});
		});
	}

	fetchKonsul = () => {
		this.setState({
			loadingListKonsul: true
		}, () => {
			api.get(`/konsul`)
			.then(result => {
				this.setState({ listKonsul: result.data, loadingListKonsul: false });
			})
			.catch(error => {
				console.log(error.response);
				this.setState({ loadingListKonsul: false });
			});
		});
	}

	pulang = () => {
		if (this.state.pasien_id !== '' && this.state.residen_id !== '' && this.state.opsiPulang !== '') {
			this.setState({
				loadingListPasien: true
			}, () => {
				api.post(`/pasien/checkout`, {
					pasien_id: this.state.pasien_id,
					residen_id: this.state.residen_id,
					opsi_pulang: this.state.opsiPulang,
				})
				.then(result => {
					this.fetchPasien();
					this.closeAlert();
				})
				.catch(error => {
					console.log(error.response);
					this.setState({ loadingListPasien: false });
					this.closeAlert();
				});
			});
		}
	}

	logout = () => {
		localStorage.removeItem('token');
		localStorage.removeItem('username');
		this.props.history.push('/');
		window.location.reload();
	}

	closeAlert = () => {
		this.setState({ openAlert: false, opsiPulang: '', pasien_id: '', residen_id: '' });
	}
	
	render() {
		return (
			<Fragment>
				<Footer />
				<Segment basic style={{ bottom: 30 }}>
					<Button onClick={this.logout}>Logout</Button>
					<Button primary as={Link} to="/input" >Input Pasien</Button>
					<Button primary as={Link} to="/residen" >Residen</Button>
					<Button primary as={Link} to="/konsul" >Konsul</Button>
					<Button primary as={Link} to="/report" >Report</Button>
					<Segment raised loading={this.state.loadingListPasien}>
						<h3>
							<span style={{ marginRight: 10 }}>List Pasien</span>
						</h3>
						<Dropdown
							placeholder='Search By DPJP'
							search selection
							options={[
								{ key: '', text: '-Pilih Semua-', value: '' },
								...this.state.listDpjp
							]}
							style={{ marginRight: 5 }}
							onChange={(e, data) => {
								if (data.value !== '') {
									this.setState({
										loadingListPasien: true
									}, () => {
										api.get(`/pasien/dpjp/${data.value}`)
										.then(async result => {
											for (let i = 0; i < result.data.length; i++) {
												await this.setState({ ['selectKamar'+result.data[i].id]: result.data[i].kamar_id })
												await this.setState({ ['selectResiden'+result.data[i].id]: result.data[i].residen_id })
												await this.setState({ ['selectStatus'+result.data[i].id]: result.data[i].status_id })
												await this.setState({ ['keterangan'+result.data[i].id]: result.data[i].keterangan })
												await this.setState({ ['bed'+result.data[i].id]: result.data[i].bed })
											}
											this.setState({ listPasien: result.data, loadingListPasien: false });
										})
										.catch(error => {
											console.log(error.response);
											this.setState({ loadingListPasien: false });
										});
									});
								} else {
									this.fetchPasien();
								}
							}}
						/>
						<Dropdown
							placeholder='Search By Status'
							search selection
							options={[
								{ key: '', text: '-Pilih Semua-', value: '' },
								...this.state.listStatus
							]}
							style={{ marginRight: 5 }}
							onChange={(e, data) => {
								if (data.value !== '') {
									this.setState({
										loadingListPasien: true
									}, () => {
										api.get(`/pasien/status/${data.value}`)
										.then(async result => {
											for (let i = 0; i < result.data.length; i++) {
												await this.setState({ ['selectKamar'+result.data[i].id]: result.data[i].kamar_id })
												await this.setState({ ['selectResiden'+result.data[i].id]: result.data[i].residen_id })
												await this.setState({ ['selectStatus'+result.data[i].id]: result.data[i].status_id })
												await this.setState({ ['keterangan'+result.data[i].id]: result.data[i].keterangan })
												await this.setState({ ['bed'+result.data[i].id]: result.data[i].bed })
											}
											this.setState({ listPasien: result.data, loadingListPasien: false });
										})
										.catch(error => {
											console.log(error.response);
											this.setState({ loadingListPasien: false });
										});
									});
								} else {
									this.fetchPasien();
								}
							}}
						/>
						<Button primary onClick={() => {
							this.fetchPasien();
						}}>Semua</Button>
						<Button primary onClick={() => {
							this.setState({
								listPasien: [],
								loadingListPasien: true
							}, () => {
								api.get(`/pasien/jejaring`)
								.then(async result => {
									for (let i = 0; i < result.data.length; i++) {
										await this.setState({ ['selectKamar'+result.data[i].id]: result.data[i].kamar_id })
										await this.setState({ ['selectResiden'+result.data[i].id]: result.data[i].residen_id })
										await this.setState({ ['selectStatus'+result.data[i].id]: result.data[i].status_id })
										await this.setState({ ['keterangan'+result.data[i].id]: result.data[i].keterangan })
										await this.setState({ ['bed'+result.data[i].id]: result.data[i].bed })
									}
									this.setState({ listPasien: result.data, loadingListPasien: false });
								})
								.catch(error => {
									console.log(error.response);
									this.setState({ loadingListPasien: false });
								});
							});
						}}>Jejaring</Button>
						<Button primary onClick={() => {
							this.setState({
								listPasien: [],
								loadingListPasien: true
							}, () => {
								api.get(`/pasien/nonjejaring`)
								.then(async result => {
									for (let i = 0; i < result.data.length; i++) {
										await this.setState({ ['selectKamar'+result.data[i].id]: result.data[i].kamar_id })
										await this.setState({ ['selectResiden'+result.data[i].id]: result.data[i].residen_id })
										await this.setState({ ['selectStatus'+result.data[i].id]: result.data[i].status_id })
										await this.setState({ ['keterangan'+result.data[i].id]: result.data[i].keterangan })
										await this.setState({ ['bed'+result.data[i].id]: result.data[i].bed })
									}
									this.setState({ listPasien: result.data, loadingListPasien: false });
								})
								.catch(error => {
									console.log(error.response);
									this.setState({ loadingListPasien: false });
								});
							});
						}}>Non Jejaring</Button>
						<Message negative style={{ display: this.state.messagePulang }}>
							<Message.Header>Maafkan Kami</Message.Header>
							<p>{this.state.messagePulangText}</p>
						</Message>
						<Table celled>
							<Table.Header>
								<Table.Row>
									<Table.HeaderCell>Pasien</Table.HeaderCell>
									<Table.HeaderCell>Kamar & Bed</Table.HeaderCell>
									{
										localStorage.getItem('username') === 'admin' ?
											<Table.HeaderCell>Residen</Table.HeaderCell>
										: ''
									}
									<Table.HeaderCell>Status & Keterangan</Table.HeaderCell>
									<Table.HeaderCell>Diagnosa</Table.HeaderCell>
									<Table.HeaderCell>Action</Table.HeaderCell>
								</Table.Row>
							</Table.Header>
							<Table.Body>
								{
									this.state.listPasien.map((item, index) => (
										<Table.Row key={index} verticalAlign='top'>
											<Table.Cell>
												{index + 1}. {item.kamar_nama}<br />
												{item.nama} [{item.no_rekmedis}], {item.sex}, {item.usia}th<br />
												Dx: {JSON.parse(item.diagnosa).map((item2, index2) => (
													<span key={index2}>{item2.name}, </span>
												))}<br />
												Dpjp: {item.dpjp_nama}<br />
												{item.tgl}
											</Table.Cell>
											<Table.Cell>
												<div style={{ marginBottom: 5 }}>
													<Dropdown
														placeholder='Kamar'
														search
														selection
														options={this.state.listKamar}
														onChange={(e, data) => {
															this.setState({
																loadingListPasien: true
															}, () => {
																api.put(`/pasien/kamar/${item.id}/${data.value}`)
																.then(result => {
																	this.fetchPasien();
																})
																.catch(error => {
																	console.log(error.response);
																	this.setState({ loadingListPasien: false });
																});
															});
														}}
														value={this.state['selectKamar'+item.id]}
													/>
												</div>
												<Input
													action={{
														icon: 'save',
														onClick: (e, data) => {
															this.setState({
																loadingListPasien: true
															}, () => {
																api.post(`/pasien/bed`, {
																	pasien_id: item.id,
																	bed: this.state['bed'+item.id]
																})
																.then(result => {
																	this.fetchPasien();
																	this.fetchResiden();
																})
																.catch(error => {
																	console.log(error.response);
																	this.setState({ loadingListPasien: false });
																});
															});
														}
													}}
													placeholder="Bed"
													onChange={(e) => {
														this.setState({ ['bed'+item.id]: e.target.value });
													}}
													value={this.state['bed'+item.id]}
												/>
											</Table.Cell>
											{
												localStorage.getItem('username') === 'admin' ?
													<Table.Cell>
														<Dropdown
															placeholder='Residen'
															search
															selection
															options={this.state.listResiden}
															onChange={(e, data) => {
																this.setState({
																	loadingListPasien: true
																}, () => {
																	api.put(`/pasien/residen/${item.id}/${data.value}`)
																	.then(result => {
																		this.fetchPasien();
																		this.fetchResiden();
																	})
																	.catch(error => {
																		console.log(error.response);
																		this.setState({ loadingListPasien: false });
																	});
																});
															}}
															value={this.state['selectResiden'+item.id]}
														/>
													</Table.Cell>
												: ''
											}
											<Table.Cell>
												<Dropdown
													placeholder='Status'
													search
													selection
													options={this.state.listStatus}
													onChange={(e, data) => {
														this.setState({
															loadingListPasien: true
														}, () => {
															api.put(`/pasien/status/${item.id}/${data.value}`)
															.then(result => {
																this.fetchPasien();
																this.fetchResiden();
															})
															.catch(error => {
																console.log(error.response);
																this.setState({ loadingListPasien: false });
															});
														});
													}}
													value={this.state['selectStatus'+item.id]}
												/>
												<Input
													action={{
														icon: 'save',
														onClick: (e, data) => {
															this.setState({
																loadingListPasien: true
															}, () => {
																api.post(`/pasien/keterangan`, {
																	pasien_id: item.id,
																	keterangan: this.state['keterangan'+item.id]
																})
																.then(result => {
																	this.fetchPasien();
																	this.fetchResiden();
																})
																.catch(error => {
																	console.log(error.response);
																	this.setState({ loadingListPasien: false });
																});
															});
														}
													}}
													placeholder="Keterangan"
													onChange={(e) => {
														this.setState({ ['keterangan'+item.id]: e.target.value });
													}}
													value={this.state['keterangan'+item.id]}
												/>
											</Table.Cell>
											<Table.Cell>
												<Input
													action={{
														icon: 'save',
														onClick: (e, data) => {
															let diagnosa = JSON.parse(item.diagnosa);
															diagnosa.push({ name: this.state.diagnosa_aktif });
															this.setState({
																loadingListPasien: true
															}, () => {
																api.post(`/pasien/delete/diagnosa`, {
																	pasien_id: item.id,
																	diagnosa: diagnosa
																})
																.then(result => {
																	this.fetchPasien();
																	this.fetchResiden();
																	this.setState({ diagnosa_aktif: '' });
																})
																.catch(error => {
																	console.log(error.response);
																	this.setState({ loadingListPasien: false });
																	this.setState({ diagnosa_aktif: '' });
																});
															});
														}
													}}
													placeholder="Input Diagnosa"
													onChange={(e) => {
														this.setState({ diagnosa_aktif: e.target.value });
													}}
												/>
												<List divided selection>
													{
														JSON.parse(item.diagnosa).map((item2, index2) => (
															<Label key={index2}>{item2.name} <Icon name='delete' onClick={() => {
																let diagnosa = JSON.parse(item.diagnosa);
																let diagnosa2 = [];
																for (let i = 0; i < diagnosa.length; i++) {
																	if (diagnosa[i].name !== item2.name) {
																		diagnosa2.push(diagnosa[i]);
																	}
																}
																api.post(`/pasien/delete/diagnosa`, {
																	pasien_id: item.id,
																	diagnosa: diagnosa2
																})
																.then(result => {
																	this.fetchPasien();
																	this.fetchResiden();
																})
																.catch(error => {
																	console.log(error.response);
																	this.setState({ loadingListPasien: false });
																});
															}} /></Label>
														))
													}
												</List>
											</Table.Cell>
											<Table.Cell>
												<Popup
													content='Edit'
													trigger={
														<Button icon='user' color="blue"
															onClick={
																() => {
																	this.setState({
																		aktif: item,
																		openEditBiodata: true
																	})
																}
															}
														/>
													}
												/>
												<Popup
													content='Checkout'
													trigger={
														<Button icon='home' color="red"
															onClick={() => {
																if (this.state['selectResiden'+item.id] === undefined || this.state['selectResiden'+item.id] === 0 || this.state['selectResiden'+item.id] === '') {
																	this.setState({
																		messagePulang: '',
																		messagePulangText: 'Residen Belum Dipilih'
																	});
																} else {
																	let residen_id  = this.state['selectResiden'+item.id];
																	this.setState({
																		messagePulang: 'none',
																		messagePulangText: '',
																		pasien_id: item.id,
																		residen_id: residen_id,
																		openAlert: true,
																	});
																}
															}}
														/>
													}
												/>
											</Table.Cell>
										</Table.Row>
									))
								}
							</Table.Body>
						</Table>
					</Segment>
					<Segment raised loading={this.state.loadingListResiden}>
						<h3>Residen</h3>
						<Table color="yellow">
							<Table.Header>
								<Table.Row>
									<Table.HeaderCell>No.</Table.HeaderCell>
									<Table.HeaderCell>Nama</Table.HeaderCell>
									<Table.HeaderCell>Jumlah Pasien</Table.HeaderCell>
								</Table.Row>
							</Table.Header>
							<Table.Body>
								{this.state.listResidenGroup.map((item, index) => (
									<Table.Row key={index}>
										<Table.Cell>{index+1}</Table.Cell>
										<Table.Cell>{item.residen_nama}</Table.Cell>
										<Table.Cell>{item.jml}</Table.Cell>
									</Table.Row>
								))}
							</Table.Body>
						</Table>
					</Segment>
					{
						this.state.openEditBiodata ?
						<EditBiodata
							data={this.state.aktif}
							close={() => {
								this.setState({ openEditBiodata: false });
								this.fetchPasien();
							}}
						/> : null
					}
				</Segment>

				<Modal size='mini' open={this.state.openAlert} onClose={this.closeAlert}>
					<Modal.Content>
						<Dropdown
							placeholder='Opsi Pulang'
							search
							selection
							options={[
								{ key: 'lepas', text: 'Lepas Rawat', value: 'lepas' },
								{ key: 'pbj', text: 'PBJ', value: 'pbj' },
							]}
							onChange={(e, data) => {
								this.setState({ opsiPulang: data.value });
							}}
							value={this.state.opsiPulang}
						/>
					</Modal.Content>
					<Modal.Actions>
            <Button
              positive
              icon='home'
              labelPosition='right'
							content='Checkout'
							onClick={this.pulang}
            />
          </Modal.Actions>
				</Modal>
			</Fragment>
		)
	}
}

export default FormResiden
