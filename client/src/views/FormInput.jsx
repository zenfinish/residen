import React, { Component, Fragment } from 'react';
import { Segment, Table, Form, Input, Button, Label, Message, Modal, Header, Select } from 'semantic-ui-react';
import SemanticDatepicker from 'react-semantic-ui-datepickers';
import 'react-semantic-ui-datepickers/dist/react-semantic-ui-datepickers.css';
import api from 'configs/api.js';
import { tglSql, tglIndo } from 'configs/helpers.js';
import Menu from 'components/Menu.jsx';
import Footer from '../components/Footer';

class FormInput extends Component {

	state = {
		nama: '',
		no_rekmedis: '',
		tgl_lahir: new Date(),
		diagnosa: '',
		sex: '',
		dpjp: '',
		kamar: '',
		status: '',
		bed: '',

		listDpjp: [],
		listKamar: [],
		listPasien: [],
		listResiden: [],
		listResidenGroup: [],
		listKonsul: [],

		loadingDaftar: false,
		messageDaftar: 'none',
		messageDaftarText: '',

		loadingListPasien: false,
		loadingListResiden: false,
		loadingListKonsul: false,

		messagePulang: 'none',
		messagePulangText: '',

		openModal: false,

		aktif: {
			nama: '',
			no_rekmedis: '',
			diagnosa: '[]',
			tgl_lahir: '',
			usia: '',
			sex: '',
			dpjp_nama: '',
			residen_nama: '',
		}
	}
	
	componentDidMount() {
		
		api.get(`/user/cek-token`)
		.then(result => {
			this.setState({ token: true });
		})
		.catch(error => {
			this.setState({ token: false }, () => {
				this.props.history.push('/');
			});
		});
		
		api.get(`/dpjp`)
		.then(result => {
			let listDpjp = [];
			for (let i = 0; i < result.data.length; i++) {
				listDpjp.push({ key: result.data[i].id, text: result.data[i].nama, value: result.data[i].id });
			}
			this.setState({ listDpjp: listDpjp });
		})
		.catch(error => {
			console.log(error.response);
		});

		api.get(`/kamar`)
		.then(result => {
			let listKamar = [];
			for (let i = 0; i < result.data.length; i++) {
				listKamar.push({ key: result.data[i].id, text: result.data[i].nama, value: result.data[i].id });
			}
			this.setState({ listKamar: listKamar, kamar: 26 });
		})
		.catch(error => {
			console.log(error.response);
		});

		this.fetchPasien();
	}

	fetchPasien = () => {
		this.setState({
			loadingListPasien: true
		}, () => {
			api.get(`/pasien`)
			.then(async result => {
				for (let i = 0; i < result.data.length; i++) {
					await this.setState({ ['selectKamar'+result.data[i].id]: result.data[i].kamar_id })
					await this.setState({ ['selectResiden'+result.data[i].id]: result.data[i].residen_id })
				}
				this.setState({ listPasien: result.data, loadingListPasien: false });
			})
			.catch(error => {
				console.log(error.response);
				this.setState({ loadingListPasien: false });
			});
		});
	}

	simpan = (e) => {
		this.setState({
			loadingDaftar: true
		}, () => {
			if (this.state.nama === '') {
				this.setState({ loadingDaftar: false }, () => {
					this.setState({ messageDaftar: '', messageDaftarText: 'Nama Belum Diisi' });
				});
			} else if (this.state.no_rekmedis === '') {
				this.setState({ loadingDaftar: false }, () => {
					this.setState({ messageDaftar: '', messageDaftarText: 'No. Rekmedis Belum Diisi' });
				});
			} else if (this.state.tgl_lahir === '') {
				this.setState({ loadingDaftar: false }, () => {
					this.setState({ messageDaftar: '', messageDaftarText: 'tgl_lahir Belum Diisi' });
				});
			} else if (this.state.sex === '') {
				this.setState({ loadingDaftar: false }, () => {
					this.setState({ messageDaftar: '', messageDaftarText: 'Sex Belum Diisi' });
				});
			} else if (this.state.dpjp === '') {
				this.setState({ loadingDaftar: false }, () => {
					this.setState({ messageDaftar: '', messageDaftarText: 'DPJP Belum Diisi' });
				});
			} else if (this.state.kamar === '') {
				this.setState({ loadingDaftar: false }, () => {
					this.setState({ messageDaftar: '', messageDaftarText: 'Kamar Belum Diisi' });
				});
			} else if (this.state.status === '') {
				this.setState({ loadingDaftar: false }, () => {
					this.setState({ messageDaftar: '', messageDaftarText: 'Status Belum Dipilih' });
				});
			} else if (this.state.bed === '') {
				this.setState({ loadingDaftar: false }, () => {
					this.setState({ messageDaftar: '', messageDaftarText: 'Bed Belum Diisi' });
				});
			} else {
				let diagnosa = this.state.diagnosa.split(',');
				let diagnosa2 = [];
				for (let i = 0; i < diagnosa.length; i++) {
					diagnosa2.push({ name: diagnosa[i] });
				};
				api.post(`/pasien`, {
					no_rekmedis: this.state.no_rekmedis,
					nama: this.state.nama,
					tgl_lahir: this.state.tgl_lahir,
					sex: this.state.sex,
					diagnosa: diagnosa2,
					dpjp_id: this.state.dpjp,
					kamar_id: this.state.kamar,
					status_id: this.state.status,
					bed: this.state.bed,
				})
				.then(result => {
					this.setState({
						loadingDaftar: false,
						nama: '',
						no_rekmedis: '',
						tgl_lahir: '',
						diagnosa: [],
						sex: '',
						dpjp: '',
						kamar: '',
						status: '',
						bed: '',
						messageDaftar: 'none', messageDaftarText: ''
					}, () => {
						this.fetchPasien();
					});
				})
				.catch(error => {
					this.setState({ loadingDaftar: false }, () => {
						this.setState({ messageDaftar: '', messageDaftarText: error.response.data });
					});
				});
			}
		});
	}

	logout = () => {
		localStorage.removeItem('token');
		localStorage.removeItem('username');
		this.props.history.push('/');
		window.location.reload();
	}
	
	render() {
		return (
			<Fragment>
				<Footer />
				<Segment basic style={{ bottom: 30 }}>
					<Menu />
					<Segment raised>
						<h3>Input Pasien</h3>
						<Form>
							<Form.Group>
								<Form.Field
									control={Input}
									label='Nama'
									placeholder='Nama'
									onChange={(e) => {
										this.setState({ nama: e.target.value })
									}}
									value={this.state.nama}
									width={10}
								/>
								<Form.Field
									control={Input}
									label='No. Rekmedis'
									placeholder='No. Rekmedis'
									onChange={(e) => {
										this.setState({ no_rekmedis: e.target.value })
									}}
									value={this.state.no_rekmedis}
									width={2}
								/>
								<SemanticDatepicker
									label='Tgl Lahir'
									onChange={(e, data) => {
										this.setState({ tgl_lahir: tglSql(data.value) })
									}}
									format="MM-DD-YYYY"
									allowOnlyNumbers
								/>
								<Form.Field
									width={2}
								>
									<label>Sex</label>
									<Select
										options={[
											{ key: 'm', text: 'Male', value: 'm' },
											{ key: 'f', text: 'Female', value: 'f' },
											{ key: 'o', text: 'Other', value: 'o' },
										]}
										placeholder='Sex'
										onChange={(e, { value }) => {
											this.setState({ sex: value })
										}}
										value={this.state.sex}
										fluid
									/>
								</Form.Field>
							</Form.Group>
							<Form.Group>
								<Form.Field
									control={Input}
									label='Diagnosa'
									placeholder='Diagnosa (Separator ",")'
									onChange={(e) => {
										this.setState({ diagnosa: e.target.value })
									}}
									value={this.state.diagnosa}
									width={16}
								/>
							</Form.Group>
							<Form.Group>
								<Form.Field
									width={3}
								>
									<label>Status</label>
									<Select
										options={[
											{ key: '1', text: 'Dpjp', value: '1' },
											{ key: '2', text: 'Raber', value: '2' },
											{ key: '3', text: 'Sewaktu', value: '3' },
										]}
										placeholder='Status'
										onChange={(e, { value }) => {
											this.setState({ status: value })
										}}
										value={this.state.status}
										fluid
									/>
								</Form.Field>
								<Form.Field
									width={3}
								>
									<label>DPJP</label>
									<Select
										options={this.state.listDpjp}
										placeholder='DPJP'
										onChange={(e, { value }) => {
											this.setState({ dpjp: value })
										}}
										value={this.state.dpjp}
										fluid
									/>
								</Form.Field>
								<Form.Field
									width={3}
								>
									<label>Kamar</label>
									<Select
										options={this.state.listKamar}
										placeholder='Kamar'
										onChange={(e, { value }) => {
											this.setState({ kamar: value })
										}}
										value={this.state.kamar}
										fluid
									/>
								</Form.Field>
								<Form.Field
									control={Input}
									label='Bed'
									placeholder='Bed'
									onChange={(e) => {
										this.setState({ bed: e.target.value })
									}}
									value={this.state.bed}
									width={3}
								/>
							</Form.Group>
							<Form.Group style={{ marginTop: 10 }}>
								<Form.Field>
									<Button as="a" loading={this.state.loadingDaftar} onClick={this.simpan}>Daftar</Button>
									<Message negative style={{ display: this.state.messageDaftar }}>
										<Message.Header>Maafkan Kami</Message.Header>
										<p>{this.state.messageDaftarText}</p>
									</Message>
								</Form.Field>
							</Form.Group>
						</Form>
					</Segment>
					<Segment raised>
						<h3>
							<span style={{ marginRight: 10 }}>List Pasien</span>
						</h3>
						<Message negative style={{ display: this.state.messagePulang }}>
							<Message.Header>Maafkan Kami</Message.Header>
							<p>{this.state.messagePulangText}</p>
						</Message>
						<Table compact celled>
							<Table.Header>
								<Table.Row>
									<Table.HeaderCell>Nama</Table.HeaderCell>
									<Table.HeaderCell>No. Rekmedis</Table.HeaderCell>
									<Table.HeaderCell>Kamar</Table.HeaderCell>
									<Table.HeaderCell>Residen</Table.HeaderCell>
									<Table.HeaderCell>Action</Table.HeaderCell>
								</Table.Row>
							</Table.Header>
							<Table.Body>
								{
									this.state.listPasien.map((item, index) => (
										<Table.Row key={index} verticalAlign='top'>
											<Table.Cell>{item.nama}</Table.Cell>
											<Table.Cell>{item.no_rekmedis}</Table.Cell>
											<Table.Cell>{item.kamar_nama}</Table.Cell>
											<Table.Cell>{item.residen_nama}</Table.Cell>
											<Table.Cell>
												<Button icon='user' color="blue"
													onClick={
														() => {
															this.setState({
																aktif: {
																	nama: item.nama,
																	no_rekmedis: item.no_rekmedis,
																	diagnosa: item.diagnosa,
																	tgl_lahir: item.tgl_lahir,
																	usia: item.usia,
																	sex: item.sex,
																	dpjp_nama: item.dpjp_nama,
																	residen_nama: item.residen_nama,
																},
																openModal: true
															})
														}
													}
												/>
											</Table.Cell>
										</Table.Row>
									))
								}
							</Table.Body>
						</Table>
						<Modal open={this.state.openModal} onClose={() => {this.setState({ openModal: false })}} dimmer="blurring">
							<Modal.Header>Detail Pasien</Modal.Header>
							<Modal.Content scrolling>
								<Modal.Description>
									<Header>Nama</Header>
									<p>{this.state.aktif.nama}</p>
									<Header>No. Rekmedis</Header>
									<p>{this.state.aktif.no_rekmedis}</p>
									<Header>tgl_lahir</Header>
									<p>{tglIndo(this.state.aktif.tgl_lahir)}, {this.state.aktif.usia}th</p>
									<Header>Sex</Header>
									<p>{this.state.aktif.sex}</p>
									<Header>Dpjp</Header>
									<p>{this.state.aktif.dpjp_nama}</p>
									<Header>Residen</Header>
									<p>{this.state.aktif.residen_nama}</p>
									<Header>Diagnosa</Header>
									{JSON.parse(this.state.aktif.diagnosa).map((item2, index2) => (
										<div style={{ marginBottom: 2 }} key={index2}><Label>{item2.name}</Label></div>
									))}
								</Modal.Description>
							</Modal.Content>
						</Modal>
					</Segment>
				</Segment>
			</Fragment>
		)
	}
}

export default FormInput
