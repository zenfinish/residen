import React, { Component } from 'react';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import { Segment } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import Login from 'views/Login.jsx';
import Main from 'views/Main.jsx';
import FormInput from 'views/FormInput.jsx';
import FormKonsul from 'views/FormKonsul.jsx';
import FormResiden from 'views/FormResiden.jsx';
import FormReport from 'views/FormReport.jsx';

class App extends Component {
		
	render() {
		return (
			<Segment basic>
				<Router basename={process.env.REACT_APP_BASENAME}>
					<Switch>
						<Route exact path={`${process.env.PUBLIC_URL}/`} component={Login} />
						<Route path={`${process.env.PUBLIC_URL}/main`} component={Main} />
						<Route path={`${process.env.PUBLIC_URL}/input`} component={FormInput} />
						<Route path={`${process.env.PUBLIC_URL}/konsul`} component={FormKonsul} />
						<Route path={`${process.env.PUBLIC_URL}/residen`} component={FormResiden} />
						<Route path={`${process.env.PUBLIC_URL}/report`} component={FormReport} />
					</Switch>
				</Router>
			</Segment>
		)
	}
}

export default App;