import React, { Component } from 'react';
import { Menu } from 'semantic-ui-react';

class Footer extends Component {
	
	render() {
		return (
			<Menu fixed='bottom' inverted style={{ backgroundColor: 'white' }}>
				Hak Cipta &copy; &nbsp; <a href="https://verd.co.id/" target="_blank" rel="noopener noreferrer">verd.co.id</a>
			</Menu>
		)
	}
}

export default Footer
