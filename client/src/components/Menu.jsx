import React, { Component, Fragment } from 'react';
import { Button } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

class Menu extends Component {

	logout = () => {
		localStorage.removeItem('token');
		localStorage.removeItem('username');
		window.location.reload();
	}
	
	render() {
		return (
			<Fragment>
				<Button onClick={this.logout}>Logout</Button>
				<Button primary as={Link} to="/input" >Input Pasien</Button>
				{
					localStorage.getItem('username') === 'admin' ?
					<Fragment>
						<Button primary as={Link} to="/residen" >Residen</Button>
						<Button primary as={Link} to="/konsul" >Konsul</Button>
					</Fragment> : null
				}
				<Button primary as={Link} to="/report" >Report</Button>
			</Fragment>
		)
	}
}

export default Menu
