const express = require('express');
const Telegraf = require('telegraf');
// const axios = require('axios');
const cron = require('node-cron');
const mysql = require('mysql');

// const Markup = require('telegraf/markup');

require('dotenv').config();

const connMysql = mysql.createConnection({
	host: process.env.HOST_MYSQL,
	user: process.env.USER_MYSQL,
	password: process.env.PASS_MYSQL,
	database: process.env.DB_MYSQL,
});
connMysql.connect();

const app = express();
const PORT = process.env.PORT;
const bot = new Telegraf(process.env.BOT_TOKEN);

// bot.catch((err, ctx) => {
// 	console.log(`Ooops, ecountered an error for ${ctx.updateType}`, err)
// });

// const task = cron.schedule('00 04 * * *', function() {
// 	axios({
// 		method: 'get',
// 		url: `http://localhost:3001/pasien/`
// 	})
// 	.then(function (result) {
// 		let tampil = `<pre>Pasien Per Tanggal ${new Date()}
		
// `;
// 			for (let i = 0; i < result.data.length; i++) {
// 				if (result.data[i].residen_id === 'rz') {
// 					let diagnosa = JSON.parse(result.data[i].diagnosa);
// 					let diagnosa2 = '';
// 					for (let j = 0; j < diagnosa.length; j++) {
// 						diagnosa2 += `	- ${diagnosa[j].name}
// `;
// 					}
// 				let sex = 'Tidak Diketahui';
// 				if (result.data[i].sex === 'm') {
// 					sex = 'Laki-Laki';
// 				} else if (result.data[i].sex === 'f') {
// 					sex = 'Perempuan';
// 				}
// 				tampil += `${i+1}. ${result.data[i].nama}
// ${sex}, ${result.data[i].usia} thn, RM ${result.data[i].no_rekmedis}
// Diagnosa:
// ${diagnosa2}Dpjp. ${result.data[i].dpjp_nama}
// Ruang ${result.data[i].kamar_nama} bed ${result.data[i].bed}
// Status: ${result.data[i].status_nama}

// `;
// 				}
// 			}
// 			tampil += `</pre>`;
// 			bot.telegram.sendMessage(484346436, tampil, { parse_mode: 'HTML' })
// 				.catch(function(error) {
// 					console.log('masuk===', error.response)
// 				});
// 	})
// 	.catch(function (error) {
// 		bot.telegram.sendMessage(512832623, JSON.stringify(error.response))
// 			.catch(function(error) {
// 				console.log('masuk===', error.response)
// 			});
// 	});
// });

// bot.start((ctx) => {
// 	ctx.replyWithHTML(`<pre>1. Format:	"register/residen/:username/:passwordBot"
// Untuk Mendaftar Sebagai Residen.

// 2. Format: "register/operator/:username/:passwordBot"
// Untuk Mendaftar Sebagai Operator.

// 3. Format: "dpjp/:kode_dpjp/:passwordBot"
// Untuk Melihat Data Pasien Berdasarkan DPJP.

// 4. Format: "residen/:kode_dpjp/:passwordBot"
// Untuk Melihat Data Pasien Berdasarkan DPJP.

// 5. Format: "konsul/nama/:rm/ruang/diagnosa/alasan"
// Untuk Request Konsul.

// 6. Format: "konsul/:rm/selesai/:passwordBot"
// Untuk Menjawab Konsul.

// 7. Format: "konsul/:rm/raber/:kode_dpjp/:passwordBot"
// Untuk Memasukkan Pasien Konsul Ke List Pasien Aktif.

// 8. Format: "list/residen/:passwordBot"
// Untuk Melihat Seluruh Data Residen.

// 9. Format: "list/dpjp/:passwordBot"
// Untuk Melihat Seluruh Data DPJP.

// 10. Format: "list/konsul/:passwordBot"
// Untuk Minta List Pasien Konsul.</pre>`)
// 	.catch(function(error) {
// 		console.log('masuk===', error.response)
// 		// if (error.response && error.response.statusCode === 403) {
// 		//   // ...snip...
// 		// }
// 	});
// });

// bot.action('info', ({ replyWithHTML }) => {
// 	replyWithHTML(`<pre>1. Format:	"register/residen/:username/:passwordBot"
// Untuk Mendaftar Sebagai Residen.

// 2. Format: "register/operator/:username/:passwordBot"
// Untuk Mendaftar Sebagai Operator.

// 3. Format: "dpjp/:kode_dpjp/:passwordBot"
// Untuk Melihat Data Pasien Berdasarkan DPJP.

// 4. Format: "residen/:kode_dpjp/:passwordBot"
// Untuk Melihat Data Pasien Berdasarkan DPJP.

// 5. Format: "konsul/nama/:rm/ruang/diagnosa/alasan"
// Untuk Request Konsul.

// 6. Format: "konsul/:rm/selesai/:passwordBot"
// Untuk Menjawab Konsul.

// 7. Format: "konsul/:rm/raber/:kode_dpjp/:passwordBot"
// Untuk Memasukkan Pasien Konsul Ke List Pasien Aktif.

// 8. Format: "list/residen/:passwordBot"
// Untuk Melihat Seluruh Data Residen.

// 9. Format: "list/dpjp/:passwordBot"
// Untuk Melihat Seluruh Data DPJP.

// 10. Format: "list/konsul/:passwordBot"
// Untuk Minta List Pasien Konsul.</pre>`)
// 	.catch(function(error) {
// 		console.log('masuk===', error.response)
// 	});
// });

// bot.command('guguk', (ctx) => {
// 	console.log('guguk=========', ctx.update.message.entities)
// })
// 	.catch((error) => {
// 		console.log('error launch =>', error)
// 	});

// bot.use(({ update, from, reply, replyWithHTML }, next) => {
// 	let text1 = update.message.text.toLowerCase();
// 	let text = text1.split('/');
// 	if (text[0] === 'register' && text[1] === 'residen' && text[2] !== '' && text[2] !== undefined && text[3] === 'jpd88') {
// 		axios({
// 			method: 'post',
// 			url: `http://localhost:3001/user/register/residen`,
// 			data: {
// 				username: text[2],
// 				telegram: from.id,
// 				nama: from.first_name + ' ' + from.last_name,
// 				status: '1',
// 			}
// 		})
// 		.then(function (result) {
// 			reply('Register Berhasil Sebagai Residen, silahkan login dengan username dengan default password username Anda, setiap minggu pagi password akan dihapus, silahkan Masukkan Format : "password" Untuk Mendapatkan Password Baru.')
// 				.catch(function(error) {
// 					console.log('masuk===', error.response)
// 				});
// 		}).catch(function (error) {
// 			reply(error.response.data)
// 				.catch(function(error) {
// 					console.log('masuk===', error.response)
// 				});
// 		});
// 	}
// 	else if (text[0] === 'konsul' && text[1] !== '' && text[1] !== undefined && text[2] === 'selesai' && text[3] === 'jpd88') {
// 		axios({
// 			method: 'put',
// 			url: `http://localhost:3001/konsul`,
// 			data: {
// 				no_rekmedis: text[1],
// 			}
// 		})
// 		.then(function (result) {
// 			reply('Data Berhasil Diupdate.')
// 				.catch(function(error) {
// 					console.log('masuk===', error.response)
// 				});
// 		}).catch(function (error) {
// 			reply(error.response.data)
// 				.catch(function(error) {
// 					console.log('masuk===', error.response)
// 				});
// 		});
// 	}
// 	else if (text[0] === 'konsul' && text[1] !== '' && text[1] !== undefined && text[2] !== '' && text[2] !== undefined && text[3] !== '' && text[3] !== undefined && text[4] !== '' && text[4] !== undefined && (text[5] === 'raber' || text[5] === 'sewaktu')) {
// 		axios({
// 			method: 'post',
// 			url: `http://localhost:3001/konsul`,
// 			data: {
// 				nama: text[1],
// 				no_rekmedis: text[2],
// 				ruang: text[3],
// 				diagnosa: text[4],
// 				tujuan: text[5],
// 			}
// 		})
// 		.then(function (result) {
// 			reply('Request Konsul Berhasil.')
// 				.catch(function(error) {
// 					console.log('masuk===', error.response)
// 				});
// 		}).catch(function (error) {
// 			reply(error.response.data)
// 				.catch(function(error) {
// 					console.log('masuk===', error.response)
// 				});
// 		});
// 	}
// 	else if (text[0] === 'konsul' && text[1] !== '' && text[1] !== undefined && text[2] === 'raber' && text[3] !== '' && text[3] !== undefined && text[4] === 'jpd88') {
// 		axios({
// 			method: 'post',
// 			url: `http://localhost:3001/konsul/pindah`,
// 			data: {
// 				no_rekmedis: text[1],
// 				dpjp_id: text[3],
// 			}
// 		})
// 		.then(function (result) {
// 			reply('Pasien Berhasil Dipindahkan Ke List Pasien Aktif.')
// 				.catch(function(error) {
// 					console.log('masuk===', error.response)
// 				});
// 		}).catch(function (error) {
// 			reply(error.response.data)
// 				.catch(function(error) {
// 					console.log('masuk===', error.response)
// 				});
// 		});
// 	}
// 	else if (text[0] === 'list' && text[1] === 'konsul' && text[2] === 'jpd88') {
// 		axios({
// 			method: 'get',
// 			url: `http://localhost:3001/konsul`,
// 		})
// 		.then(function (result) {
// 			let tampil = `<pre>`;
// 			for (let i = 0; i < result.data.length; i++) {
// 				tampil += `
// ${i+1}. ${result.data[i].nama}, ${result.data[i].no_rekmedis}, ${result.data[i].ruang}, ${result.data[i].diagnosa}, ${result.data[i].tujuan}, ${result.data[i].waktu}
// `;
// 			}
// 			tampil += '</pre>';
// 			replyWithHTML(tampil)
// 				.catch(function(error) {
// 					console.log('masuk===', error.response)
// 				});
// 		}).catch(function (error) {
// 			reply(error.response.data)
// 				.catch(function(error) {
// 					console.log('masuk===', error.response)
// 				});
// 		});
// 	}
// 	else if (text[0] === 'register' && text[1] === 'operator' && text[2] !== '' && text[2] !== undefined && text[3] === 'ppsd') {
// 		axios({
// 			method: 'post',
// 			url: `http://localhost:3001/user/register/operator`,
// 			data: {
// 				username: text[2],
// 				telegram: from.id,
// 				nama: from.first_name + ' ' + from.last_name,
// 				status: '2',
// 			}
// 		})
// 		.then(function (result) {
// 			reply('Register Berhasil Sebagai Operator, silahkan login dengan username dengan default password username Anda, setiap minggu pagi password akan dihapus, silahkan Masukkan Format : "password" Untuk Mendapatkan Password Baru.')
// 				.catch(function(error) {
// 					console.log('masuk===', error.response)
// 				});
// 		}).catch(function (error) {
// 			reply(error.response.data)
// 				.catch(function(error) {
// 					console.log('masuk===', error.response)
// 				});
// 		});
// 	}
// 	else if (text[0] === 'list' && text[1] === 'residen' && text[2] === 'jpd88') {
// 		axios({
// 			method: 'get',
// 			url: `http://localhost:3001/residen`
// 		})
// 		.then(function (result) {
// 			let tampil = `<pre>Nama | Kode`;
// 			for (let i = 0; i < result.data.length; i++) {
// 				tampil += `
// ${result.data[i].nama} | ${result.data[i].id}`;
// 			}
// 			tampil += '</pre>';
// 			replyWithHTML(tampil)
// 				.catch(function(error) {
// 					console.log('masuk===', error.response)
// 				});
// 		}).catch(function (error) {
// 			reply(error.response.data)
// 				.catch(function(error) {
// 					console.log('masuk===', error.response)
// 				});
// 		});
// 	}
// 	else if (text[0] === 'list' && text[1] === 'dpjp' && text[2] === 'jpd88') {
// 		axios({
// 			method: 'get',
// 			url: `http://localhost:3001/dpjp`
// 		})
// 		.then(function (result) {
// 			let tampil = `<pre>Nama | Kode`;
// 			for (let i = 0; i < result.data.length; i++) {
// 				tampil += `
// ${result.data[i].nama} | ${result.data[i].id}`;
// 			}
// 			tampil += '</pre>';
// 			replyWithHTML(tampil)
// 				.catch(function(error) {
// 					console.log('masuk===', error.response)
// 				});
// 		}).catch(function (error) {
// 			reply(error.response.data)
// 				.catch(function(error) {
// 					console.log('masuk===', error.response)
// 				});
// 		});
// 	}
// 	else if (text[0] === 'residen' && text[1] !== '' && text[1] !== undefined && text[2] === 'jpd88') {
// 		axios({
// 			method: 'get',
// 			url: `http://localhost:3001/residen/aktif/${text[1]}`
// 		})
// 		.then(function (result) {
// 			let tampil = `<pre>`;
// 			for (let i = 0; i < result.data.length; i++) {
// 				let diagnosa = JSON.parse(result.data[i].diagnosa);
// 				let diagnosa2 = '';
// 				for (let j = 0; j < diagnosa.length; j++) {
// 					diagnosa2 += `	- ${diagnosa[j].name}
// `;
// 				}
// 				let sex = 'Tidak Diketahui';
// 				if (result.data[i].sex === 'm') {
// 					sex = 'Laki-Laki';
// 				} else if (result.data[i].sex === 'f') {
// 					sex = 'Perempuan';
// 				}
// 				tampil += `${i+1}. ${result.data[i].nama}
// ${sex}, ${result.data[i].usia} thn, RM ${result.data[i].no_rekmedis}
// Diagnosa:
// ${diagnosa2}Dpjp. ${result.data[i].dpjp_nama}
// Ruang ${result.data[i].kamar_nama} bed ${result.data[i].bed}
// Status: ${result.data[i].status_nama}

// `;
// 			}
// 			tampil += `</pre>`;
// 			replyWithHTML(tampil)
// 				.catch(function(error) {
// 					console.log('masuk===', error.response)
// 				});
// 		}).catch(function (error) {
// 			reply(error.response.data)
// 				.catch(function(error) {
// 					console.log('masuk===', error.response)
// 				});
// 		});
// 	}
// 	else if (text[0] === 'dpjp' && text[1] !== '' && text[1] !== undefined && text[2] === 'jpd88') {
// 		axios({
// 			method: 'get',
// 			url: `http://localhost:3001/dpjp/aktif/${text[1]}`
// 		})
// 		.then(function (result) {
// 			let tampil = `<pre>`;
// 			for (let i = 0; i < result.data.length; i++) {
// 				let diagnosa = JSON.parse(result.data[i].diagnosa);
// 				let diagnosa2 = '';
// 				for (let j = 0; j < diagnosa.length; j++) {
// 					diagnosa2 += `	- ${diagnosa[j].name}
// `;
// 				}
// 				let sex = 'Tidak Diketahui';
// 				if (result.data[i].sex === 'm') {
// 					sex = 'Laki-Laki';
// 				} else if (result.data[i].sex === 'f') {
// 					sex = 'Perempuan';
// 				}
// 				tampil += `${i+1}. ${result.data[i].nama}
// ${sex}, ${result.data[i].usia} thn, RM ${result.data[i].no_rekmedis}
// Diagnosa:
// ${diagnosa2}Dpjp. ${result.data[i].dpjp_nama}
// Ruang ${result.data[i].kamar_nama} bed ${result.data[i].bed}
// Status: ${result.data[i].status_nama}

// `;
// 			}
// 			tampil += `</pre>`;
// 			replyWithHTML(tampil)
// 				.catch(function(error) {
// 					console.log('masuk===', error.response)
// 				});
// 		}).catch(function (error) {
// 			reply(error.response.data)
// 				.catch(function(error) {
// 					console.log('masuk===', error.response)
// 				});
// 		});
// 	}
// 	else {
// 		bot.telegram.sendMessage(from.id, `Format Tidak Sesuai!`, { reply_markup: Markup.inlineKeyboard([
// 			Markup.callbackButton(`Info`, `info`)
// 		]) })
// 		.catch((error) => {
// 			console.log('keluar========', error)
// 		});
// 	}
// })
// 	.catch((error) => {
// 		console.log('error use =>', error)
// 	});

const task = cron.schedule('00 06 * * *', function() {
	connMysql.query(`
		SELECT
			a.id,
			a.nama,
			a.no_rekmedis,
			DATE_FORMAT(a.tgl_lahir, '%Y-%m-%d') AS tgl_lahir,
			ROUND(DATEDIFF(CURRENT_DATE, STR_TO_DATE(a.tgl_lahir, '%Y-%m-%d'))/365, 0) AS usia,
			DATE_FORMAT(a.tgl, '%d %M %Y %T') AS tgl,
			a.sex,
			a.diagnosa,
			a.kamar_id,
			a.residen_id,
			a.bed,
			a.keterangan,
			a.status_id,
			b.nama AS dpjp_nama,
			c.nama AS kamar_nama,
			d.nama AS status_nama,
			e.nama AS residen_nama,
			e.telegram
		FROM pasien a
		LEFT JOIN data_dpjp b ON a.dpjp_id = b.id
		LEFT JOIN data_kamar c ON a.kamar_id = c.id
		LEFT JOIN data_status d ON a.status_id = d.id
		LEFT JOIN data_residen e ON a.residen_id = e.id
		WHERE a.tgl_checkout = '0000-00-00 00:00'
		ORDER BY a.id DESC
	`, function(error, result) {
		let data = result;
		for (let i = 0; i < data.length; i++) {
			if (data[i].telegram) {
				bot.telegram.sendMessage(data[i].telegram, `
<pre>${data[i].kamar_nama}, ${data[i].bed}
${data[i].nama}, ${data[i].sex}, ${data[i].usia}th, ${data[i].no_rekmedis}
Dx: ${
	JSON.parse(data[i].diagnosa).map((item2, index2) => {
		return `${item2.name}, `;
	})
}
Status: ${data[i].status_nama}
Konsulen: ${data[i].dpjp_nama}
Keterangan: ${data[i].keterangan}</pre>
				`, { parse_mode: 'HTML' })
				.catch(function(error) {
					console.log('masuk===', error.response)
				});
			}
		}
	});
});

bot.hears('daftar', (ctx) => {
	connMysql.query(`INSERT INTO data_user (telegram, nama) VALUES ('${ctx.update.message.from.id}', '${ctx.update.message.from.first_name} ${ctx.update.message.from.last_name}')`, function(error, result) {
		if (error) {
			ctx.reply(`Gagal Daftar : ${error.sqlMessage}`);
		} else {
			ctx.reply('Berhasil Daftar');
		}
	});
})
.catch((error) => {
	console.log('error launch =>', error)
});

bot.hears('cek', (ctx) => {
	connMysql.query(`
		SELECT
			a.id,
			a.nama,
			a.no_rekmedis,
			DATE_FORMAT(a.tgl_lahir, '%Y-%m-%d') AS tgl_lahir,
			ROUND(DATEDIFF(CURRENT_DATE, STR_TO_DATE(a.tgl_lahir, '%Y-%m-%d'))/365, 0) AS usia,
			DATE_FORMAT(a.tgl, '%d %M %Y %T') AS tgl,
			a.sex,
			a.diagnosa,
			a.kamar_id,
			a.residen_id,
			a.bed,
			a.keterangan,
			a.status_id,
			b.nama AS dpjp_nama,
			c.nama AS kamar_nama,
			d.nama AS status_nama,
			e.nama AS residen_nama,
			e.telegram
		FROM pasien a
		LEFT JOIN data_dpjp b ON a.dpjp_id = b.id
		LEFT JOIN data_kamar c ON a.kamar_id = c.id
		LEFT JOIN data_status d ON a.status_id = d.id
		LEFT JOIN data_residen e ON a.residen_id = e.id
		WHERE a.tgl_checkout = '0000-00-00 00:00'
		ORDER BY a.id DESC
	`, function(error, result) {
		let data = result;
		for (let i = 0; i < data.length; i++) {
			if (data[i].telegram) {
				bot.telegram.sendMessage(data[i].telegram, `
<pre>${data[i].kamar_nama}, ${data[i].bed}
${data[i].nama}, ${data[i].sex}, ${data[i].usia}th, ${data[i].no_rekmedis}
Dx: ${
	JSON.parse(data[i].diagnosa).map((item2, index2) => {
		return `${item2.name}, `;
	})
}
Status: ${data[i].status_nama}
Konsulen: ${data[i].dpjp_nama}
Keterangan: ${data[i].keterangan}</pre>
				`, { parse_mode: 'HTML' })
				.catch(function(error) {
					console.log('masuk===', error.response)
				});
			}
		}
	});
})
.catch((error) => {
	console.log('error launch =>', error)
});

bot.launch()
.then((success) => {
	console.log('Sukses masuk=================')
})
.catch((error) => {
	console.log('error launch =>', error)
});

app.get('/', function(req, res){
	res.status(200).json('Masuk Pak Eko');
});
 
app.listen(PORT, function() {
  console.log('Running on port ', PORT);
});

